#include "gpu-new-forward.h"

/* Error Handler */
cudaError_t ProjErrorHandler(int line, const char* func, const char* file, ProjErrorCode inErr, const char *errormess,...)
{
  char  ebuf[PROJ_MAX_BUF_SIZE], estr[PROJ_MAX_BUF_SIZE];

  if (errormess) {
    std::va_list Argp;
    va_start(Argp, errormess);
    std::vsnprintf(ebuf, PROJ_MAX_BUF_SIZE, errormess, Argp);
    va_end(Argp);
    /* No error checking becuase cyclical defines */
  }
  switch (inErr) {
  case PROJ_ERR_CUDA:
    {
      const char str2[] = "CUDA call has generated an error\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  case PROJ_ERR_CUBLAS:
    {
      const char str2[] = "CUBLAS call has generated an error\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  case PROJ_ERR_POINTER:
    {
      const char str2[] = "Argument NULL pointer\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  case PROJ_ERR_ARGRANGE:
    {
      const char str2[] = "Argument out of range\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  case PROJ_ERR_INCONST:
    {
      const char str2[] = "Inconsistent data generated\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  default:
    {
      const char str2[] = "An unamed error has occured\0";
      std::strncpy(estr, str2, sizeof(estr));
    }
    break;
  }

  std::cout<<print_bar__<<" ERROR "<<print_bar__<<std::endl;
  std::cout<<"ERROR: "<<estr<<std::endl;
  std::cout<<"ERROR: "<<ebuf<<std::endl;
  std::cout<<"ERROR: See stack below for error trace\nERROR:"<<std::endl;
  std::cout<<"ERROR: "<<func<<"() line "<<line<<" in file "<<file<<std::endl;
  ProjStackView();
  std::cout<<print_bar__<<" ERROR "<<print_bar__<<std::endl;
  std::cout<<std::flush;
  throw std::runtime_error(estr);
  return(cudaErrorUnknown);
}

/* Custom Backtrace Support */
cudaError_t ProjStackInitialize(void)
{
  _p_ProjStack *stk;

  //stk = (_p_ProjStack*)malloc(sizeof(_p_ProjStack));
  stk = new _p_ProjStack;
  stk->nregf = 0;
  for (int i = 0; i < PROJ_MAX_STACK_SIZE; ++i) {
    stk->line[i] = -1;
    stk->func[i] = NULL;
    stk->file[i] = NULL;
  }
  ProjStack = stk;
  return(cudaSuccess);
}

cudaError_t ProjStackFinalize(void)
{
  if (ProjStack) {
    //free(ProjStack);
    delete ProjStack;
    ProjStack = NULL;
  }
  return(cudaSuccess);
}

void ProjStackPushFunc(int line, const char* func, const char *file)
{
  if (ProjStack) {
    if (ProjStack->nregf < PROJ_MAX_STACK_SIZE) {
      ProjStack->line[ProjStack->nregf] = line;
      ProjStack->func[ProjStack->nregf] = func;
      ProjStack->file[ProjStack->nregf] = file;
      ProjStack->nregf++;
    }
  }
  return;
}

void ProjStackPopFunc(void)
{
  if (ProjStack) {
    if (ProjStack->nregf > 0) {
      /* Decrement first in case nregf == PROJ_MAX_STACK_SIZE */
      ProjStack->nregf--;
      ProjStack->line[ProjStack->nregf] = -1;
      ProjStack->func[ProjStack->nregf] = NULL;
      ProjStack->file[ProjStack->nregf] = NULL;
    }
  }
  return;
}

void ProjStackView(void)
{
  for (int i = ProjStack->nregf-1; i >= 0; --i) {
    std::cout<<"ERROR: "<<ProjStack->func[i]<<"() line "<<ProjStack->line[i]<<" in file "<<ProjStack->file[i]<<std::endl;
  }
  return;
}

/* General Purpose */
cudaError_t ProjPrintf(const char *m,...)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  char  buf[PROJ_MAX_BUF_SIZE];
  int   len;

  if (!m) SETERRC(PROJ_ERR_POINTER,"Invalid char pointer");
  std::va_list Argp;
  va_start(Argp, m);
  len = std::vsnprintf(buf, PROJ_MAX_BUF_SIZE, m, Argp);
  va_end(Argp);
  if (len < 0 || len > PROJ_MAX_BUF_SIZE) SETERRC(PROJ_ERR_ARGRANGE,"Format string exceeded maximum length %d",PROJ_MAX_BUF_SIZE);
  std::cout<<buf;
#else
  (void)(m);
#endif
  ProjFunctionReturn(cudaSuccess);
}

/* Custom Profiling Support */
static int   ProjProfileRuncount;
static float ProjProfileTime, ProjProfileTimeLo, ProjProfileTimeHi;

cudaError_t ProjProfileInitialize(cudaEvent_t *start, cudaEvent_t *stop)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;

    cerr = cudaEventCreate(start);CHKERRCE(cerr);
    cerr = cudaEventCreate(stop);CHKERRCE(cerr);
    cerr = ProjProfileReset();CHKERRCE(cerr);
  }
#else
  (void)(start);
  (void)(stop);
#endif
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileReset(void)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;

    cerr = cudaDeviceSynchronize();CHKERRCE(cerr);
    ProjProfileRuncount = 0;
    ProjProfileTime = ProjProfileTimeHi = 0.0f;
    ProjProfileTimeLo = 999999999.9f;
  }
#endif
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileBegin(cudaEvent_t start, cudaStream_t stream)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;
    cerr = cudaEventRecord(start, stream);CHKERRCE(cerr);
  }
#endif
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileEnd(cudaEvent_t start, cudaEvent_t stop, cudaStream_t stream)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;
    float       time = 0.0f;

    cerr = cudaEventRecord(stop, stream);CHKERRCE(cerr);
    cerr = cudaEventSynchronize(stop);CHKERRCE(cerr);
    cerr = cudaEventElapsedTime(&time, start, stop);CHKERRCE(cerr);
    ProjProfileTimeLo =	ProjProfileTimeLo <= time ? ProjProfileTimeLo : time;
    ProjProfileTimeHi =	ProjProfileTimeHi >= time ? ProjProfileTimeHi : time;
    ProjProfileTime += time;
    ++ProjProfileRuncount;
  }
#endif
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileView(void)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;

    cerr = ProjPrintf("Total number of iterations %d\nTotal elapsed time %f (ms)\nAverage time/run %f (ms) Lo %f (ms), Hi %f (ms)\n", ProjProfileRuncount, ProjProfileTime, ProjProfileTime/(float)std::max(ProjProfileRuncount, 1), ProjProfileTimeLo, ProjProfileTimeHi);CHKERRCE(cerr);
  }
#endif
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileGetInfo(int *rc, float *rt, float *lo, float *hi)
{
  ProjFunctionBegin;
  if (rc) *rc = ProjProfileRuncount;
  if (rt) *rt = ProjProfileTime;
  if (lo) *lo = ProjProfileTimeLo;
  if (hi) *hi = ProjProfileTimeHi;
  ProjFunctionReturn(cudaSuccess);
}

cudaError_t ProjProfileFinalize(cudaEvent_t start, cudaEvent_t stop)
{
  ProjFunctionBegin;
#if defined(PROJ_USE_DEBUG)
  {
    cudaError_t cerr;

    cerr = cudaEventDestroy(start);CHKERRCE(cerr);
    cerr = cudaEventDestroy(stop);CHKERRCE(cerr);
    cerr = ProjProfileReset();CHKERRCE(cerr);
  }
#endif
  ProjFunctionReturn(cudaSuccess);
}

/* Garbage */
template <typename T>
class ProjMatrix
{
public:
  T& operator[](const unsigned i) {return mat[i];}
  constexpr int GetRows(void) const {return rows;}
  constexpr int GetCols(void) const {return cols;}
  constexpr int GetPitchLen(void) const {return int(pitch/sizeof(T));}
  constexpr int GetPitch(void) const {return pitch;}
  T* GetMat(void) {
    cudaStreamWaitEvent(stream, DtoH, 0);
    return mat;
  }
  T* GetDeviceMat(void) {
    cudaStreamWaitEvent(stream, HtoD, 0);
    offloadMask = DEVICE_CURRENT;
    return d_mat;
  }
  cudaError_t Synchronize(void)
  {
    cudaError_t cerr;

    ProjFunctionBegin;
    switch (offloadMask) {
    case HOST_CURRENT:
      cerr = SyncDevice();CHKERRCE(cerr);
      break;
    case DEVICE_CURRENT:
      cerr = SyncHost();CHKERRCE(cerr);
    default:
      break;
    }
    ProjFunctionReturn(cudaSuccess);
  }
  cudaError_t SyncDevice(void)
  {
    cudaError_t cerr;

    ProjFunctionBegin;
    if (offloadMask != HOST_CURRENT) ProjFunctionReturn(cudaSuccess);
    if (!d_mat) {
      cerr = cudaMallocPitch(&d_mat, &pitch, cols*sizeof(T), rows);CHKERRCE(cerr);
      cerr = cudaMemset2DAsync(d_mat, pitch, 0, cols*sizeof(T), rows, stream);CHKERRCE(cerr);
    }
    cerr = cudaMemcpy2DAsync(d_mat, pitch, mat, cols*sizeof(T), cols*sizeof(T), rows, cudaMemcpyHostToDevice, stream);CHKERRCE(cerr);
    cerr = cudaEventRecord(HtoD, stream);CHKERRCE(cerr);
    /* Both up to date */
    offloadMask = BOTH_CURRENT;
    ProjFunctionReturn(cudaSuccess);
  }
  cudaError_t SyncHost(void)
  {
    cudaError_t cerr;

    ProjFunctionBegin;
    if (offloadMask != DEVICE_CURRENT) ProjFunctionReturn(cudaSuccess);
    cerr = cudaMemcpy2DAsync(mat, cols*sizeof(T), d_mat, pitch, cols*sizeof(T), rows, cudaMemcpyDeviceToHost, stream);CHKERRCE(cerr);
    cerr = cudaEventRecord(DtoH, stream);CHKERRCE(cerr);
    /* Both up to date */
    offloadMask = BOTH_CURRENT;
    ProjFunctionReturn(cudaSuccess);
  }
  cudaError_t Generate(bool random=false)
  {
    ProjFunctionBegin;
    for (int i = 0; i < rows; ++i) {
      for (int j = 0; j < cols; ++j) {
	if (random) mat[i*cols+j] = (float)(rand()%7);
	else mat[i*cols+j] = (float)(j);
      }
    }
    ProjFunctionReturn(cudaSuccess);
  }
  cudaError_t Transpose(void)
  {
    T *tmat = NULL;

    ProjFunctionBegin;
    tmat = new T[rows*cols];
    std::memcpy((void *)tmat, (const void *)mat, rows*cols*sizeof(T));
    for (int i = 0; i < cols; ++i) {
      for (int j = 0; j < rows; ++j) {
	tmat[i*rows+j] = mat[j*cols+i];
      }
    }
    std::memcpy((void *)mat, (const void *)tmat, rows*cols*sizeof(T));
    delete[] tmat;
    ProjFunctionReturn(cudaSuccess);
  }
  /* Declare in class to force inline */
  ProjMatrix(const int R, const int C, const char name[], bool transpose=false)
  {
    size_t      namelen = std::strlen(name);
    cudaError_t cerr;

    ProjFunctionBegin;
    cerr = cudaMallocHost(&mat, R*C*sizeof(T), cudaHostAllocWriteCombined);CHKERRC(cerr);
    d_mat = NULL;
    cerr = cudaStreamCreate(&stream);CHKERRC(cerr);
    cerr = cudaEventCreateWithFlags(&DtoH, cudaEventDisableTiming);CHKERRC(cerr);
    cerr = cudaEventCreateWithFlags(&HtoD, cudaEventDisableTiming);CHKERRC(cerr);
    mat_name = new char[namelen+1];
    std::memset(mat_name, 0, (namelen+1)*sizeof(char));
    std::memcpy(mat_name, name, namelen*sizeof(char));
    rows = R;
    cols = C;
    cerr = Generate(true);CHKERRC(cerr);
    if (transpose) {
      cerr = Transpose();CHKERRC(cerr);
      rows = C;
      cols = R;
    }
    /* Record event so host can be indexed */
    cerr = cudaEventRecord(DtoH, stream);CHKERRC(cerr);
    /* Host up to date */
    offloadMask = HOST_CURRENT;
    ProjFunctionReturnVoid;
  }
  ~ProjMatrix() {
    cudaError_t cerr;

    ProjFunctionBegin;
    cerr = cudaFreeHost(mat);CHKERRC(cerr);
    delete[] mat_name;
    if (d_mat) {cerr = cudaFree(d_mat);CHKERRC(cerr);}
    cerr = cudaStreamDestroy(stream);CHKERRC(cerr);
    cerr = cudaEventDestroy(HtoD);CHKERRC(cerr);
    cerr = cudaEventDestroy(DtoH);CHKERRC(cerr);
    ProjFunctionReturnVoid;
  }
private:
  T		*mat, *d_mat;
  int		 rows, cols;
  char		*mat_name;
  size_t         pitch;
  int            pitchLen;
  cudaEvent_t    HtoD, DtoH;
  cudaStream_t	 stream;
  enum mask {
    DEVICE_CURRENT = -1,
    BOTH_CURRENT = 0,
    HOST_CURRENT = 1
  } offloadMask;
};
