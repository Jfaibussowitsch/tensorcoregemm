#ifndef SRC_LAYER_GPU_NEW_FORWARD_H
#define SRC_LAYER_GPU_NEW_FORWARD_H

//#define PROJ_USE_DEBUG true

#include <stdexcept>
#include <cstdarg>
#include <cstring>
#include <iostream>
#include <cassert>
#include <fstream>
#include <cuda_runtime_api.h>
#include <cuda_fp16.h>
#include <mma.h>

typedef int ProjErrorCode;
static const char print_bar__[] = "========================================";

/* Useful defines */
#define PROJ_HOST               __host__
#define PROJ_HOST_SI		static PROJ_HOST inline
#define PROJ_DEVICE             __device__ __forceinline__
#define PROJ_DEVICE_SI          static PROJ_DEVICE
#define PROJ_HOSTDEVICE         __global__

#define ProjUnlikely(x)		__builtin_expect(!!(x), 0)
#define ProjLikely(x)		__builtin_expect(!!(x), 1)

/* Constants */
#define PROJ_MAX_REG_PTRS       10
#define PROJ_MAX_STREAMS        2
#define PROJ_MAX_STACK_SIZE     32
#define PROJ_MAX_BUF_SIZE	1024
#if defined(PROJ_USE_DEBUG)
#define PROJ_PROFILE_REPS       1000
#else
#define PROJ_PROFILE_REPS       1
#endif

#define MAX_M                   16
#define MAX_C                   4
#define MASK_WIDTH              7
#define BLOCK_SIZE              16
#define BLOCK_SIZE_X            BLOCK_SIZE
#define BLOCK_SIZE_Y            BLOCK_SIZE
#define BLOCK_SIZE_Z            1
#define KERNEL_SIZE             (MAX_M*MAX_C*MASK_WIDTH*MASK_WIDTH)

#define WMMA_M                  16
#define WMMA_N                  16
#define WMMA_K                  16
#define WARP_SIZE               32
#define TC_BLOCK_SIZE_X         64
#define TC_BLOCK_SIZE_Y         2
#define TC_COMPUTE_SIZE_X       TC_BLOCK_SIZE_X
#define TC_COMPUTE_SIZE_Y       (TC_BLOCK_SIZE_Y*WARP_SIZE)

#define TC_WARP_COLS            (TC_BLOCK_SIZE_X/WARP_SIZE)
#define TC_WARP_ROWS            TC_BLOCK_SIZE_Y
#define TC_BLOCK_WARPS          (TC_WARP_ROWS*TC_WARP_COLS)
#define TC_VEC_LOAD             8
#define TC_SHMEM_SKEW           16

#define UNROLL_A_BOUND          1024
#define UNROLL_B_BOUND          896
#define TC_CONST_A_SIZE         6400
#define WARP_MASK               0xffffffff

/* Error codes */
#define PROJ_ERR_CUDA		8675300
#define PROJ_ERR_CUBLAS         8675301
#define PROJ_ERR_POINTER	8675302
#define PROJ_ERR_ARGRANGE	8675303
#define PROJ_ERR_INCONST        8675304

#define Y4D(i3, i2, i1, i0)   y[(i3) * (M * H_out * W_out) + (i2) * (H_out * W_out) + (i1) * (W_out) + i0]
#define X4D(i3, i2, i1, i0)   x[(i3) * (C * H * W) + (i2) * (H * W) + (i1) * (W) + i0]
#define K4D(i3, i2, i1, i0)   k[(i3) * (C * K * K) + (i2) * (K * K) + (i1) * (K) + i0]
#define GX4D(i3, i2, i1, i0)  __ldg(&(X4D((i3),(i2),(i1),(i0))))
#define CK4D(i3, i2, i1, i0)  constd_k[(i3)*(C*K*K)+(i2)*(K*K)+(i1)*(K)+i0]
#define CK4DH(i3, i2, i1, i0) constd_k_h[(i3)*(C*K*K)+(i2)*(K*K)+(i1)*(K)+i0]
#define T2D(i1,i0)            tile[(i1)*(BLOCK_SIZE_X+K-1)+(i0)]
#define XO3D(i2,i1,i0)        x_o[(i2)*(C*K*K*H_out*W_out)+(i1)+(i0)]

struct _p_ProjStack {
  int        nregf;
  int        line[PROJ_MAX_STACK_SIZE];
  const char *func[PROJ_MAX_STACK_SIZE];
  const char *file[PROJ_MAX_STACK_SIZE];
};
static _p_ProjStack *ProjStack;

PROJ_HOST cudaError_t ProjStackInitialize(void);
PROJ_HOST cudaError_t ProjStackFinalize(void);
PROJ_HOST void ProjStackPushFunc(int,const char*,const char*);
PROJ_HOST void ProjStackPopFunc(void);
PROJ_HOST void ProjStackView(void);
PROJ_HOST cudaError_t ProjErrorHandler(int,const char*,const char*,ProjErrorCode,const char*,...);
PROJ_HOST cudaError_t ProjPrintf(const char*,...);
PROJ_HOST cudaError_t ProjProfileReset(void);
PROJ_HOST cudaError_t ProjProfileInitialize(cudaEvent_t*,cudaEvent_t*);
PROJ_HOST cudaError_t ProjProfileReset(void);
PROJ_HOST cudaError_t ProjProfileBegin(cudaEvent_t,cudaStream_t);
PROJ_HOST cudaError_t ProjProfileEnd(cudaEvent_t,cudaEvent_t,cudaStream_t);
PROJ_HOST cudaError_t ProjProfileView(void);
PROJ_HOST cudaError_t ProjProfileGetInfo(int*,float*,float*,float*);
PROJ_HOST cudaError_t ProjProfileFinalize(cudaEvent_t,cudaEvent_t);

/* Check for gcc since empty va_arg lists are some __funky__ bois */
#if defined(__GNUC__)
#define SETERRC(cerr,s,...)             return(ProjErrorHandler(__LINE__,__FUNCTION__,__FILE__,(ProjErrorCode)(cerr),s,##__VA_ARGS__))
#define SETERRV(cerr,s,...)             do{ProjErrorHandler(__LINE__,__FUNCTION__,__FILE__,(ProjErrorCode)(cerr),s,##__VA_ARGS__);return;} while (0)
#else /* __GNUC__ */
#error "oopsie woopsie theres been a bit of a fucksy wucksy and gcc-compatible compiler isn't being used"
#endif /* __GNUC__ */
#define CHKERRC(cerr)							\
  do {									\
    if (ProjUnlikely((cerr) != cudaSuccess)) {				\
      const char *n = cudaGetErrorName(cerr);				\
      const char *d = cudaGetErrorString(cerr);				\
      SETERRV(PROJ_ERR_CUDA,"CUDAErrorCode %d %s: %s",(ProjErrorCode)(cerr),n,d); \
    }									\
  } while (0)
#define CHKERRCE(cerr)							\
  do {									\
    if (ProjUnlikely((cerr) != cudaSuccess)) {				\
      const char *n = cudaGetErrorName(cerr);				\
      const char *d = cudaGetErrorString(cerr);				\
      SETERRC(PROJ_ERR_CUDA,"CUDAErrorCode %d -> %s: %s",(ProjErrorCode)(cerr),n,d); \
    }									\
  } while (0)
#if defined(PROJ_USE_DEBUG)
#define CHKERRK()							\
  do {									\
    cudaError_t cerr_k_ = cudaDeviceSynchronize();CHKERRC(cerr_k_);	\
  } while(0)
#define ProjFunctionBegin				\
  do {							\
    ProjStackPushFunc(__LINE__,__FUNCTION__,__FILE__);	\
  } while (0)
#define ProjFunctionReturnVoid			\
  do {						\
    ProjStackPopFunc();				\
    return;					\
  } while (0)
#define ProjFunctionReturn(err)			\
  do {						\
    ProjStackPopFunc();				\
    return(err);				\
  } while (0)
#define ProjValidPointer(ptr,arg)					\
  do {									\
    if (!ptr) {SETERRC(PROJ_ERR_POINTER,"Null pointer: paramter %d",arg);} \
  } while (0)
#define	ProjAssertC(cond)						\
  do {if(!(cond)) SETERRC(PROJ_ERR_INCONST,"Assertion failed\n");} while(0);
#define	ProjAssertV(cond)						\
  do {if(!(cond)) SETERRV(PROJ_ERR_INCONST,"Assertion failed\n");} while(0);
#define ProjAssert(cond)          assert(cond);
#else /* PROJ_USE_DEBUG */
#define CHKERRK()
#define ProjFunctionBegin
#define ProjFunctionReturnVoid	  return
#define ProjFunctionReturn(err)   return(err)
#define ProjValidPointer(ptr,arg) (void)(ptr)
#define ProjAssertC(cond)
#define ProjAssertV(cond)
#define ProjAssert(cond)
#endif /* PROJ_USE_DEBUG */

class GPUInterface
{
public:
  PROJ_HOST void get_device_properties();
  PROJ_HOST void conv_forward_gpu(float *host_y, const float *host_x, const float *host_k, const int B, const int M, const int C, const int H, const int W, const int K);
  template <int>
  PROJ_HOST cudaError_t conv_forward_gpu_TC(float*,const float*,const float*,const int,const int,const int,const int,const int);
  template <int>
  PROJ_HOST cudaError_t conv_forward_gpu_SH(float*,const float*,const float*,const int,const int,const int,const int,const int);
  PROJ_HOST cudaError_t arrayConvert(half*,const float*,unsigned int,cudaStream_t,int);
  /* Declare in class to force inline */
  PROJ_HOST cudaError_t ProjRegisterPointer(void *ptr, size_t size)
  {
    cudaPointerAttributes attr;
    cudaError_t           cerr;

    ProjFunctionBegin;
    if (cudaPointerGetAttributes(&attr, ptr) == cudaErrorInvalidValue) {
      cudaGetLastError();
      if (ProjUnlikely(nreg == PROJ_MAX_REG_PTRS)) {
	/* ran out of pointer space, unregister first and shuffle everyone down */
	cerr = cudaHostUnregister(regptrs[0]);CHKERRCE(cerr);
	for (int i = 1; i < PROJ_MAX_REG_PTRS; ++i) regptrs[i-1] = regptrs[i];
	--nreg;
      }
      cerr = cudaHostRegister(ptr, size, cudaHostRegisterPortable);CHKERRCE(cerr);
      cerr = ProjPrintf("Pointer %p registered\n", ptr);CHKERRCE(cerr);
      regptrs[++nreg] = ptr;
    }
    ProjFunctionReturn(cudaSuccess);
  }
  PROJ_HOST cudaError_t ProjUnregisterPointer(void *ptr)
  {
    cudaError_t cerr;

    ProjFunctionBegin;
    if (!ptr) ProjFunctionReturn(cudaSuccess);
    cerr = ProjPrintf("Pointer %p unregistered\n", ptr);CHKERRCE(cerr);
    cerr = cudaHostUnregister(ptr);CHKERRCE(cerr);
    ProjFunctionReturn(cudaSuccess);
  }
  GPUInterface();
  ~GPUInterface();
private:
  cudaEvent_t  copyEvent, computeEvent;
  cudaStream_t stream[PROJ_MAX_STREAMS];
  int          nreg;
  void         *regptrs[PROJ_MAX_REG_PTRS];
  cudaEvent_t  start, stop;
  cudaDeviceProp dProp;
  float        *h_constd_k;
  half         *h_constd_k_h, *h_constd_a;
};

template <typename T>
cudaError_t ProjPrintArray(const T *arr, size_t rows, size_t cols, int arrnum)
{
  ProjFunctionBegin;
  std::cout<<print_bar__<<"BEGIN KERNEL "<<arrnum<<print_bar__<<std::endl;
  std::cout<<"[";
  for (int i = 0; i < rows; ++i) {
    std::cout<<"[";
    for (int j = 0; j < cols; ++j) {
      if (j == cols-1) {
	std::printf("%f", arr[i*cols+j]);
      } else {
	std::printf("%f, ", arr[i*cols+j]);
      }
    }
    if (i == rows-1) {
      std::cout<<"]";
    } else {
      std::cout<<"],";
    }
  }
  std::cout<<"]"<<std::endl;
  std::cout<<print_bar__<<"END KERNEL "<<arrnum<<" "<<print_bar__<<std::endl;
  ProjFunctionReturn(cudaSuccess);
}

PROJ_HOSTDEVICE void MatMatTransMult(float*__restrict__,const half*__restrict__,const half*__restrict__,int,int,int,size_t,size_t);
PROJ_HOSTDEVICE void MatMatTransMultClass(float*__restrict__,const half*__restrict__,const half*__restrict__,int,int,int,size_t,size_t);
#endif /* SRC_LAYER_GPU_NEW_FORWARD_H */
