#include "gpu-new-forward.h"

#if (0)
extern __constant__ half constZeroVector[TC_VEC_LOAD];

/* CLASS */
template <typename OperandT=half, typename ResultT=float, int numBuffers=4>
class MMAContext {
private:
  enum struct MatrixType {matA, matB, matC, matSH};
  enum struct DimType    {Row, Col};
  struct __builtin_align__(16) WarpContext {
    const unsigned int M, N, warp, lane;
    PROJ_DEVICE constexpr WarpContext(const uint3 tidx=uint3{1,1,1}, const uint3 bidx=uint3{1,1,1})
      : M(TC_WARP_ROWS*((bidx.x*TC_BLOCK_SIZE_X+tidx.x)/WARP_SIZE)),
      N(TC_WARP_COLS*(bidx.y*TC_BLOCK_SIZE_Y+tidx.y)),
      warp((tidx.y*TC_BLOCK_SIZE_X+tidx.x)/WARP_SIZE),
      lane(tidx.x >= WARP_SIZE ? tidx.x%WARP_SIZE : tidx.x)
      {}
  };
  template <MatrixType mtype>
  struct __builtin_align__(8) MatrixPosition {
  private:
    template <MatrixType ptype, DimType dtype>
    PROJ_DEVICE	unsigned int constructPos(const WarpContext& ctx) const
    {
      switch(ptype) {
      case MatrixType::matA:
	return (dtype == DimType::Row ? ctx.M*WMMA_M : 0);
      case MatrixType::matB:
	return (dtype == DimType::Row ? 0 : ctx.N*WMMA_N);
      case MatrixType::matC:
	return (dtype == DimType::Row ? ctx.M*WMMA_M : ctx.N*WMMA_N);
      case MatrixType::matSH:
	return (dtype == DimType::Row ? __fast_mod(ctx.M, 2*TC_WARP_ROWS)*WMMA_M : __fast_mod(ctx.N, 2*TC_WARP_COLS)*WMMA_N);
      default: break;
      }
      return(0);
    }
  public:
    const unsigned int row, col;

    PROJ_DEVICE constexpr MatrixPosition(const WarpContext& ctx)
      :	row(constructPos<mtype, DimType::Row>(ctx)),
      col(constructPos<mtype, DimType::Col>(ctx))
    {}
  };

  const WarpContext                        WarpCtx;
  const	MatrixPosition<MatrixType::matA>   aPos;
  const	MatrixPosition<MatrixType::matB>   bPos;
  const	MatrixPosition<MatrixType::matC>   cPos;
  const	MatrixPosition<MatrixType::matSH>  shPos;
  const uint3                              tidx, bidx, dims;
  const size_t                             pitchOp, pitchRes;
  const OperandT                           *aPtr, *bPtr;
  ResultT                                  *cPtr, *shPtr;

  template <MatrixType ptype, typename PointerT>
  PROJ_DEVICE const PointerT* constructPtr(const PointerT *ptr) const
  {
    switch(ptype) {
    case MatrixType::matA:
      return(ptr);
    case MatrixType::matB:
      return(reinterpret_cast<const PointerT*>(reinterpret_cast<const char*>(ptr)+(size_t)(bidx.z*dims.y*pitchOp)));
    case MatrixType::matC:
      return(reinterpret_cast<const PointerT*>(reinterpret_cast<const char*>(ptr)+(size_t)(bidx.z*dims.x*pitchRes)+cPos.row*pitchRes+WarpCtx.lane*pitchRes)+cPos.col);
    case MatrixType::matSH:
      return(ptr);
    default: break;
    }
    return(NULL);
  }
  template <MatrixType ptype, typename PointerT>
  PROJ_DEVICE PointerT* constructPtr(PointerT *ptr) const
  {
    return(const_cast<PointerT*>(const_cast<const MMAContext*>(this)->constructPtr<ptype>(ptr)));
  }

public:
  PROJ_DEVICE constexpr MMAContext(const OperandT *ptra, const OperandT *ptrb, ResultT *ptrc, ResultT* ptrsh, int M, int N, int K, size_t pitchO, size_t pitchR)
    :  tidx(threadIdx), bidx(blockIdx), dims(make_uint3(M, N, K)),
       WarpCtx(this->tidx, this->bidx), pitchOp(pitchO), pitchRes(pitchR),
       aPos(this->WarpCtx), bPos(this->WarpCtx),
       cPos(this->WarpCtx), shPos(this->WarpCtx),
       aPtr(constructPtr<MatrixType::matA>(ptra)),
       bPtr(constructPtr<MatrixType::matB>(ptrb)),
       cPtr(constructPtr<MatrixType::matC>(ptrc)),
       shPtr(constructPtr<MatrixType::matSH>(ptrsh))
  {}
  PROJ_DEVICE_SI void SetupFragments(void);
  template <bool tcM, bool tcN>
  PROJ_DEVICE void Run(MMAContext<OperandT,ResultT,numBuffers>&);
};

template <typename OperandT=half, typename ResultT=float, int numBuffers=4>
PROJ_DEVICE void MMAContext::Run(MMAContext<OperandT,ResultT,numBuffers>& ctx)
{
  const int  bx     = bidx.x, by = bidx.y, tx = tidx.x, ty = tidx.y;
  const int  warpId = (ty*TC_BLOCK_SIZE_X+tx)/WARP_SIZE, laneId = __fast_mod(tx, WARP_SIZE);
  const int  warpM  = TC_WARP_ROWS*((bx*TC_BLOCK_SIZE_X+tx)/WARP_SIZE);
  const int  warpN  = TC_WARP_COLS*(by*TC_BLOCK_SIZE_Y+ty);
  const int  aRow   = warpM*WMMA_M, bCol = warpN*WMMA_N, cCol = warpN*WMMA_N, cRow = warpM*WMMA_M;
  const int  aSHRow = __fast_mod(warpM, 2*TC_WARP_ROWS)*WMMA_M;
  const int  bSHRow = __fast_mod(warpN, 2*TC_WARP_COLS)*WMMA_N;
  int        announceChannel, blockChannel;

  using namespace nvcuda;
  wmma::fragment<wmma::matrix_a,WMMA_M,WMMA_N,WMMA_K,half,wmma::row_major> a_sh_frag[TC_WARP_ROWS];
  wmma::fragment<wmma::matrix_b,WMMA_M,WMMA_N,WMMA_K,half,wmma::col_major> b_sh_frag[TC_WARP_COLS];
  wmma::fragment<wmma::accumulator,WMMA_M,WMMA_N,WMMA_K,float> acc_sh_frag[TC_BLOCK_WARPS];

  static_assert((numBuffers <= 4) && (numBuffers >= 0));
  if (tidx.z) {
    announceChannel = 0, blockChannel = numBuffers;
  } else {
#pragma unroll
    for (int i = 0; i < TC_BLOCK_WARPS; ++i) wmma::fill_fragment(acc_sh_frag[i], 0.0f);
    announceChannel = numBuffers, blockChannel = 0;
  }
  /*
    Force ptxas to optimize registers for a *single* loop rather than 15. This saves
    around 2 ms, but worth it I guess.
  */
#pragma unroll(1)
  for (int k = 0; k < K; k += WMMA_K) {
    /*
      CTA is split into two groups: loaders and consumers. Loaders will continuously feed
      consumers new data. This is done semi-asynchronously by having both groups announce
      to one another when they have finished with a particular batch of data. Loaders need
      not perform these checks for the first numBuffers set of batches, as they are writing
      to "fresh" memory locations.
    */
    if (threadIdx.z) {
      const half *aPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(A)+aRow*pitch+laneId*pitch)+k;
      const half *bPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(B)+(size_t)((bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS)))*pitch)+laneId*pitch)+k;
      /*
	For each wmma we need to load in a 64*WMMA_K shm tile = 512 elements. But we have
	only 64 loading threads, so every thread must get 16 coalesced entries. First half
	of warps load A, last half load B, but use vectorized load/stores

	After numBuffers loads, loader threads must wait until all consumer threads have used a
	particular batch of shared memory.
      */
      if (k >= numBuffers*WMMA_K) __barrier_blocking(blockChannel, TC_BLOCK_BARRIER);
      if (warpId < (TC_BLOCK_WARPS/2)) {
	/* If M is divisible by TC_COMPUTE_SIZE, we can always skip this check */
	if (tcM || (aRow+laneId < M)) {
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(aPtr+i*TC_VEC_LOAD));
	  }
	} else if (!k) {
	  /* Load zero vector exactly once */
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	  }
	}
      } else {
	/* Similarly, if N is divisble by WMMA_N this time, we can always skip this check */
	if (tcN || (bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS))+laneId < N)) {
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(bPtr+i*TC_VEC_LOAD));
	  }
	} else if (!k) {
	  /* Also load zero vector exactly once */
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	  }
	}
      }
      /*
	Announce to consumer threads that global load is complete, can continue loading
	next chunk immediately (assuming consumers allow it).
      */
      __barrier_nonblocking(announceChannel, TC_BLOCK_BARRIER);
      /*
	Perform predicated modulo within the loops to update which channel the loader
	threads next announce successful loads to.
      */
      announceChannel = __fast_mod(announceChannel+1, nuBuffers);
      /*
	Only need to update the blocking channel after the first numBuffers number of global
	loads, as threads are not overwriting previous loads until then.
      */
      if (k >= numBuffers*WMMA_K) blockChannel = numBuffers+__fast_mod(blockChannel+1, numBuffers);
    } else {
      /*
	Consumer threads must wait until loader threads have all produced this batch's
	data. Given the slow (and unfortunately not neatly page-aligned) global memory
	loads, this is the biggest bottleneck.
      */
      __barrier_blocking(blockChannel, TC_BLOCK_BARRIER);
      if (aRow < M && bCol < N) {
#pragma unroll
	for (int i = 0; i < TC_WARP_ROWS; ++i) {
	  /* Load A fragment for every row only once */
	  wmma::load_matrix_sync(a_sh_frag[i], TC_SHMEM_MAT_A(blockChannel)+aSHRow*WMMA_K+i*WMMA_M*WMMA_K, WMMA_K);
#pragma unroll
	  for (int j = 0; j < TC_WARP_COLS; ++j) {
	    /* Loop through column chunks of B, loading only on the first pass */
	    if (!i) wmma::load_matrix_sync(b_sh_frag[j], TC_SHMEM_MAT_B(blockChannel)+bSHRow*WMMA_K+j*WMMA_K*WMMA_N, WMMA_K);
	    wmma::mma_sync(acc_sh_frag[i*TC_WARP_ROWS+j], a_sh_frag[i], b_sh_frag[j], acc_sh_frag[i*TC_WARP_ROWS+j]);
	  }
	}
      }
      /* Consumers announce to producers they are done with this batch */
      __barrier_nonblocking(announceChannel, TC_BLOCK_BARRIER);
      /*
	Here they must update both channels __every__ time. Loader threads will begin
	checking their block channel during the second pass, but this corresponds to the
	consumer threads first pass announcements.
      */
      announceChannel = numBuffers+__fast_mod(announceChannel+1, numBuffers);
      blockChannel = __fast_mod(blockChannel+1, numBuffers);
    }
  }
  /*
    Catch all non-loader threads for final stores. Loader threads take the highway outta
    here.
  */
  if (threadIdx.z) asm volatile ("exit;");
  __barrier_blocking(15, blockDim.x*blockDim.y);
  /*
    Store to shared memory. Note that if K is well-aligned (i.e. K%16 == 0), we could
    simply store directly to global memory here. However, since the shared to global store
    in the main kernel will likely use vectorized stores (something not possible from
    wmmma without writing this directly in PTX) this double store is acceptable.
  */
  if (cRow < M && cCol < N && !threadIdx.z) {
#pragma unroll
    for (int i = 0; i < TC_WARP_ROWS; ++i) {
#pragma unroll
      for (int j = 0; j < TC_WARP_COLS; ++j) {
	wmma::store_matrix_sync(TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+i*WMMA_M*TC_COMPUTE_SIZE_X+j*WMMA_K, acc_sh_frag[i*TC_WARP_COLS+j], TC_COMPUTE_SIZE_X, wmma::mem_row_major);
      }
    }
  }
  return;
}

template <bool tcM, bool tcN>
PROJ_HOSTDEVICE __launch_bounds__(TC_BLOCK_SIZE_X*TC_BLOCK_SIZE_Y*TC_BLOCK_SIZE_Z)
void MatMatTransMultClass(float *__restrict__ C, const half *__restrict__ A, const half *__restrict__ B, const int M, const int N, const int K, const size_t pitchK, const size_t pitchC)
{
  __shared__ float shmC[TC_COMPUTE_SIZE_X*TC_COMPUTE_SIZE_Y];

  MMAContext<half,float>  ctx(A, B, C, shmC, M, N, K, pitchK, pitchC);

  MMAContext::Run(ctx);
  //MatMatTransMult_TC<tcM, tcN, 4>(shmC, A, bPtr, M, N, K, pitchK);
  if (cRow+laneId < M && cCol < N && !threadIdx.z) {
    const int   aSHRow  = __fast_mod(warpM, 2*TC_WARP_ROWS)*WMMA_M;
    const int   bSHRow  = __fast_mod(warpN, 2*TC_WARP_ROWS)*WMMA_N;
    const int   cColRem = min(N-cCol, TC_COMPUTE_SIZE_X/TC_WARP_COLS);
    const float *shcPtr = TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+laneId*TC_COMPUTE_SIZE_X;
    float       *cPtr   = reinterpret_cast<float*>(reinterpret_cast<char*>(C)+(size_t)(bz*M*pitchC)+cRow*pitchC+laneId*pitchC)+cCol;

    /*
      Since every warp writes to it's own quadrant of shm, we need not sync the whole
      CTA. However due to voltas independant divergent thread scheduler we also can't
      assume every threads memory transaction has finished, so we must sync here.
    */
    __syncwarp(WARP_MASK);
    /*
      If N is already divisible by WMMA_K then no need to check alignment, everyone
      automatically has an entire line to copy.
    */
    if (tcN) goto max_aligned;
    switch(N%(sizeof(float4)/sizeof(float))) {
      /* 16 byte aligned, use fastest vector instructions */
    case 0:
      /* We are 16 byte aligned, but is there enough to copy? If not, drop to 8 byte */
      if (!(cColRem%(sizeof(float4)/sizeof(float)))) {
      max_aligned:
#pragma unroll
	for (int i = 0; i < cColRem; i += (sizeof(float4)/sizeof(float))) {
	  *(reinterpret_cast<float4*>(cPtr+i)) = *(reinterpret_cast<const float4*>(shcPtr+i));
	}
	break;
      }
      /* 12 byte aligned, use float3. This is mostly untested and may well write OOB */
    case sizeof(float3)/sizeof(float):
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float3)/sizeof(float))) {
	*(reinterpret_cast<float3*>(cPtr+i)) = *(reinterpret_cast<const float3*>(shcPtr+i));
      }
      break;
      /* 8 byte aligned, use float2 vector */
    case sizeof(float2)/sizeof(float):
      /* No need to check here, we check above for odd so if we are here, we are good2go */
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float2)/sizeof(float))) {
	*(reinterpret_cast<float2*>(cPtr+i)) = *(reinterpret_cast<const float2*>(shcPtr+i));
      }
      break;
      /* Not aligned at all, 4 byte aligned, or have odd number of entries :( */
    default:
#pragma unroll
      for (int i = 0; i < cColRem; ++i) cPtr[i] = shcPtr[i];
      break;
    }
  }
  return;
}
#endif
