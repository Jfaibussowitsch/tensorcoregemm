#if (0)
#include "gpu-new-forward.h"

#define TC_SHMEM_MAT_A  (reinterpret_cast<half*>(shmC))
#define TC_SHMEM_MAT_B  (TC_SHMEM_MAT_A+(WMMA_K*TC_COMPUTE_SIZE_X))
#define TC_SHMEM_MAT_C  shmC

extern __constant__ half constZeroVector[TC_VEC_LOAD];

template <bool tcM, bool tcN, size_t shcs>
PROJ_DEVICE_SI void MatMatTransMult_TC(float (&shmC)[shcs], const half *__restrict__ A, const half *__restrict__ B, int M, int N, int K, size_t pitch)
{
  const int  bx     = blockIdx.x, by = blockIdx.y, tx = threadIdx.x, ty = threadIdx.y;
  const int  warpId = (ty*TC_BLOCK_SIZE_X+tx)/WARP_SIZE, laneId = tx%WARP_SIZE;
  const int  warpM  = TC_WARP_ROWS*((bx*TC_BLOCK_SIZE_X+tx)/WARP_SIZE);
  const int  warpN  = TC_WARP_COLS*(by*TC_BLOCK_SIZE_Y+ty);
  const int  aRow   = warpM*WMMA_M, bCol = warpN*WMMA_N;
  const int  cCol   = warpN*WMMA_N, cRow = warpM*WMMA_M;
  const int  aSHRow = (warpM%(2*TC_WARP_ROWS))*WMMA_M;
  const int  bSHRow = (warpN%(2*TC_WARP_COLS))*WMMA_N;
  const half *aPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(A)+aRow*pitch+laneId*pitch);
  const half *bPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(B)+(size_t)((bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS)))*pitch)+laneId*pitch);

  using namespace nvcuda;
  wmma::fragment<wmma::matrix_a,WMMA_M,WMMA_N,WMMA_K,half,wmma::row_major> a_sh_frag[TC_WARP_ROWS];
  wmma::fragment<wmma::matrix_b,WMMA_M,WMMA_N,WMMA_K,half,wmma::col_major> b_sh_frag[TC_WARP_COLS];
  wmma::fragment<wmma::accumulator,WMMA_M,WMMA_N,WMMA_K,float> acc_sh_frag[TC_BLOCK_WARPS];

#pragma unroll
  for (int i = 0; i < TC_BLOCK_WARPS; ++i) wmma::fill_fragment(acc_sh_frag[i], 0.0f);
  /* Loop through all rows of A and Columns of B.T */
#pragma unroll
  for (int k = 0; k < K; k += WMMA_K) {
    /*
      For each wmma we need to load in a 64*WMMA_K shm tile = 512 elements. But we have
      only 64 threads, so every thread must get 16 coalesced entries. First half of warps
      load A, last half load B, but use vectorized load/stores
    */
    /*
      Given the heavy sync's (that every warp must participate in) we can possibly get
      away with not syncing here. More general solution would be to double buffer, but
      were already limited by the monstrous amount of shared memory required here
    */
    __syncthreads();
    if (warpId < (TC_BLOCK_WARPS/2)) {
      /* If N is divisible by TC_COMPUTE_SIZE, we can always skip this check */
      if (tcM || (aRow+laneId < M)) {
#pragma unroll
	for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	  *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(aPtr+i*TC_VEC_LOAD));
	}
      } else if (!k) {
	/* Load zero vector exactly once */
#pragma unroll
	for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	  *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	}
      }
    } else {
      /* Similarly, if N is divisble by WMMA_N this time, we can always skip this check */
      if (tcN || (bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS))+laneId < N)) {
#pragma unroll
	for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	  *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(bPtr+i*TC_VEC_LOAD));
	}
      } else if (!k) {
	/* Also load zero vector exactly once */
#pragma unroll
	for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	  *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	}
      }
    }
    __syncthreads();
    if (aRow < M && bCol < N) {
#pragma unroll
      for (int i = 0; i < TC_WARP_ROWS; ++i) {
	/* Load A fragment for every row only once */
	wmma::load_matrix_sync(a_sh_frag[i], TC_SHMEM_MAT_A+aSHRow*WMMA_K+i*WMMA_M*WMMA_K, WMMA_K);
#pragma unroll
	for (int j = 0; j < TC_WARP_COLS; ++j) {
	  /* Loop through column chunks of B, loading only on the first pass */
	  if (!i) wmma::load_matrix_sync(b_sh_frag[j], TC_SHMEM_MAT_B+bSHRow*WMMA_K+j*WMMA_K*WMMA_N, WMMA_K);
	  wmma::mma_sync(acc_sh_frag[i*TC_WARP_ROWS+j], a_sh_frag[i], b_sh_frag[j], acc_sh_frag[i*TC_WARP_ROWS+j]);
	}
      }
    }
    aPtr += WMMA_K;
    bPtr += WMMA_K;
  }
  /*
    Store to shared memory. Note that if K is well-aligned (i.e. K == KTC), we could
    simply store directly to global memory here. However, since the shared to global store
    in the main kernel will likely use vectorized stores (something not possible from
    wmmma without writing this directly in PTX) this double store is acceptable
  */
  __syncthreads();
  if (cRow < M && cCol < N) {
#pragma unroll
    for (int i = 0; i < TC_WARP_ROWS; ++i) {
#pragma unroll
      for (int j = 0; j < TC_WARP_COLS; ++j) {
	wmma::store_matrix_sync(TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+i*WMMA_M*TC_COMPUTE_SIZE_X+j*WMMA_K, acc_sh_frag[i*TC_WARP_COLS+j], TC_COMPUTE_SIZE_X, wmma::mem_row_major);
      }
    }
  }
  return;
}

template <bool tcM, bool tcN>
PROJ_HOSTDEVICE __launch_bounds__(TC_BLOCK_SIZE_X*TC_BLOCK_SIZE_Y)
void MatMatTransMult(float *__restrict__ C, const half *__restrict__ A, const half *__restrict__ B, int M, int N, int K, size_t pitchK, size_t pitchC)
{
  __shared__ float shmC[TC_COMPUTE_SIZE_X*TC_COMPUTE_SIZE_Y];

  const int   bx    = blockIdx.x, by = blockIdx.y, bz = blockIdx.z;
  const int   tx    = threadIdx.x, ty = threadIdx.y, laneId = tx%WARP_SIZE;
  const int   warpM = TC_WARP_ROWS*((bx*TC_BLOCK_SIZE_X+tx)/WARP_SIZE);
  const int   warpN = TC_WARP_COLS*(by*TC_BLOCK_SIZE_Y+ty);
  const int   cCol  = warpN*WMMA_N, cRow = warpM*WMMA_M;
  const half  *bPtr = reinterpret_cast<const half*>(reinterpret_cast<const char*>(B)+(size_t)(bz*N*pitchK));

  MatMatTransMult_TC<tcM, tcN>(shmC, A, bPtr, M, N, K, pitchK);
  if (cRow+laneId < M && cCol < N) {
    const int   aSHRow  = (warpM%(2*TC_WARP_ROWS))*WMMA_M;
    const int   bSHRow  = (warpN%(2*TC_WARP_ROWS))*WMMA_N;
    const int   cColRem = min(N-cCol, TC_COMPUTE_SIZE_X/TC_WARP_COLS);
    const float *shcPtr = TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+laneId*TC_COMPUTE_SIZE_X;
    float       *cPtr   = reinterpret_cast<float*>(reinterpret_cast<char*>(C)+(size_t)(bz*M*pitchC)+cRow*pitchC+laneId*pitchC)+cCol;

    __syncwarp(WARP_MASK);
    /*
      If N is already divisible by WMMA_K then no need to check alignment, everyone
      automatically has an entire line to copy
    */
    if (tcN) goto max_aligned;
    switch(N%(sizeof(float4)/sizeof(float))) {
      /* 16 byte aligned, use fastest vector instructions */
    case 0:
      /* we are 16 byte aligned, but is there enough to copy? If not, drop to 8 byte */
      if (!(cColRem%(sizeof(float4)/sizeof(float)))) {
      max_aligned:
#pragma unroll
	for (int i = 0; i < cColRem; i += (sizeof(float4)/sizeof(float))) {
	  *(reinterpret_cast<float4*>(cPtr+i)) = *(reinterpret_cast<const float4*>(shcPtr+i));
	}
	break;
      }
    case sizeof(float3)/sizeof(float):
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float3)/sizeof(float))) {
	*(reinterpret_cast<float3*>(cPtr+i)) = *(reinterpret_cast<const float3*>(shcPtr+i));
      }
      break;
      /* 8 byte aligned, use float2 vector */
    case sizeof(float2)/sizeof(float):
      /* No need to check here, we check above for odd so if we are here, we are good2go */
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float2)/sizeof(float))) {
	*(reinterpret_cast<float2*>(cPtr+i)) = *(reinterpret_cast<const float2*>(shcPtr+i));
      }
      break;
      /* not aligned at all, or 4 byte aligned :(, or have odd number of entries */
    default:
#pragma unroll
      for (int i = 0; i < cColRem; ++i) cPtr[i] = shcPtr[i];
      break;
    }
  }
  return;
}
#endif
