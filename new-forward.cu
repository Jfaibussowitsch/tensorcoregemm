#include "gpu-new-forward.h"
#include "device-util.h"

#define BLOCK_RESHUFFLE
#define CTA_CHANNEL             15
#define TC_BLOCK_SIZE_Z         2
#define TC_BLOCK_BARRIER        (TC_BLOCK_SIZE_X*TC_BLOCK_SIZE_Y*TC_BLOCK_SIZE_Z)
#define TC_BUFF_SIZE            (WMMA_K*TC_COMPUTE_SIZE_X+WMMA_K*TC_COMPUTE_SIZE_Y)
#define TC_SHMEM_MAT_A(buf__a)  (reinterpret_cast<half*>(shmC)+((buf__a)*TC_BUFF_SIZE))
#define TC_SHMEM_MAT_B(buf__b)  (TC_SHMEM_MAT_A(buf__b)+(WMMA_K*TC_COMPUTE_SIZE_X))
#define TC_SHMEM_MAT_C          shmC

static bool ProjDevicePropPrinted = false, zeroVecInit = false;

__constant__ float constd_k[KERNEL_SIZE];
__constant__ half  constd_k_h[KERNEL_SIZE];
__constant__ half  constd_a[TC_CONST_A_SIZE];
__constant__ half  constZeroVector[TC_VEC_LOAD];

/* ===================== TENSOR CORE SETUP KERNELS ========================= */
/* Functionally im2col */
template <int K>
PROJ_HOSTDEVICE __launch_bounds__(UNROLL_B_BOUND)
void setupBMatrixDB(half *__restrict__ x_o, const float *__restrict__ x, const int C, const int H, const int W, const int H_out, const int W_out, const size_t maxlen)
{
  extern __shared__ half shm[];

  const int tx     = threadIdx.x, ty = threadIdx.y, bx = blockIdx.x, by = blockIdx.y;
  const int bdx    = blockDim.x, bdy = blockDim.y, shOffset = ty*2*bdx;
  const int colIdx = bx*bdx+tx, c = colIdx/(K*K), p = (colIdx/K)%K, q = colIdx%K;
  const int yOffset = (bdy-1)*W_out*bdx, vtx = tx*(sizeof(float4)/sizeof(half));
  half      *xPtr  = x_o+(by*H_out+ty)*W_out*maxlen;
  int       flipflop = 0;

  /*
    Can get away with not syncthreads here since we sync threads below, also cull unecessary
    threads/warps so that loop is as clean as possible.
  */
  shm[tx] = half(0);
  shm[blockDim.x+tx] = half(0);
  if (colIdx >= C*K*K) asm volatile ("exit;");
  /*
    Every blockIdx.y has 1 matrix, every column is assigned to a threadIdx.y, and every
    row is split into bdx chunks, with every thread handling 1 element. Data unfortunately
    must be read sequentially from global memory as there is no guarantee it is
    vector-aligned.
  */
#pragma unroll
  for (int h = ty; h < H_out; h += bdy) {
#pragma unroll
    for (int w = 0; w < W_out; ++w) {
      if (!w || (q == MASK_WIDTH-1)) {
        shm[shOffset+flipflop*bdx+tx] = __float2half(GX4D(by,c,h+p,w+q));
      } else {
        shm[shOffset+flipflop*bdx+tx] = shm[shOffset+(1-flipflop)*bdx+tx+1];
      }
      __syncthreads();
      /*
	Shared memory on the other hand is vector aligned, and so is the output. It
	doesn't matter if we write past C*K*K as long as we don't write past bdx
	since bdx-C*K*K is extra padding which we are (re) filling with zeros
	from shm.
      */
      if (vtx < C*K*K) {
        /* Stream row to global memory */
        *(reinterpret_cast<float4*>(xPtr+vtx)) = *(reinterpret_cast<const float4*>(shm+shOffset+vtx+flipflop*bdx));
        /* Increment output pointer */
        xPtr += bdx;
      }
      /* Toggle the flipflop to use next half of shared mem */
      flipflop = 1-flipflop;
    }
    /* Only streaming threads need to update this */
    if (vtx < C*K*K) xPtr += yOffset;
  }
  return;
}

template <int K>
PROJ_HOSTDEVICE __launch_bounds__(UNROLL_B_BOUND)
void setupBMatrix(half *__restrict__ x_o, const float *__restrict__ x, int C, int H, int W, int H_out, int W_out, size_t pitch, size_t maxlen)
{
  constexpr int vecLen = sizeof(float4)/sizeof(half);
  extern __shared__ half shm[];

  const int  tx = threadIdx.x, ty = threadIdx.y, bx = blockIdx.x, by = blockIdx.y;
  const int  blockCol = bx*blockDim.x, colIdx = blockCol+tx, c = colIdx/(K*K);
  const int  p = __fast_mod(colIdx/K, K), q = __fast_mod(colIdx, K);
  half       *xPtr = reinterpret_cast<half*>(reinterpret_cast<char*>(x_o)+(size_t)(by*H_out*W_out*pitch)+(size_t)(ty*W_out*pitch)), *shOffset = shm+ty*blockDim.x*2;
  int        flipflop = 0;

  /* Can get away with not syncthreads here since we sync threads below */
  shOffset[tx] = half(0);
  shOffset[blockDim.x+tx] = half(0);
  /*
    Every blockIdx.y has 1 matrix, every column is assigned to a threadIdx.y, and every
    row is split into blockDim.x chunks, with every thread handling 1 element. Data
    unfortunately must be read sequentially from global memory as there is no guarantee it
    is vector-aligned.

    Also cull unecessary threads/warps so that loop is as clean as possible.
  */
  if (colIdx >= C*K*K) asm volatile ("exit;");
#pragma unroll
  for (int h = ty; h < H_out; h += blockDim.y) {
#pragma unroll
    for (int w = 0; w < W_out; ++w) {
      shOffset[flipflop*blockDim.x+tx] = __float2half(GX4D(by,c,h+p,w+q));
      //if (w != W_out-1) __prefetch_global_l1(&(GX4D(by,c,h+p,w+1+q)));
      __barrier_blocking(ty, blockDim.x);
      /*
	Shared memory on the other hand is vector aligned, and so is the output. It
	doesn't matter if we write past C*K*K as long as we don't write past maxlen
	since maxlen-C*K*K is extra padding which we are (re) filling with zeros
	from shm.
      */
      if (blockCol+vecLen*tx < maxlen) {
	/* Stream row to global memory */
	*(reinterpret_cast<float4*>(xPtr+blockCol+vecLen*tx)) = *(reinterpret_cast<const float4*>(shOffset+vecLen*tx+flipflop*blockDim.x));
	/* Increment output pointer */
	xPtr = reinterpret_cast<half*>(reinterpret_cast<char*>(xPtr)+pitch);
      }
      /* Toggle the flipflop to use next half of shared mem */
      flipflop = 1-flipflop;
    }
    /* Only streaming threads need to update this */
    if (blockCol+vecLen*tx < maxlen) {
      xPtr = reinterpret_cast<half*>(reinterpret_cast<char*>(xPtr)+(blockDim.y-1)*W_out*pitch);
    }
  }
  return;
}

PROJ_HOSTDEVICE __launch_bounds__(UNROLL_A_BOUND)
void setupAMatrix(half *__restrict__ out, const float *__restrict__ input, int width, size_t pitch)
{
  const int    tx = threadIdx.x, bx = blockIdx.x, by = blockIdx.y, bdx = blockDim.x;
  const float  *iPtr = input+by*width;
  half         *oPtr = reinterpret_cast<half*>(reinterpret_cast<char*>(out)+(size_t)(by*pitch));

  if ((size_t)(bx*bdx)+tx < width) oPtr[bx*bdx+tx] = __float2half(__ldg(&(iPtr[bx*bdx+tx])));
  return;
}
/* ===================== TENSOR CORE COMPUTE KERNELS ======================= */
/* NO CLASS */
template <bool tcM, bool tcN, int nBuff, size_t shcs>
PROJ_DEVICE_SI void GEMMOp(float (&shmC)[shcs], const half *__restrict__ A, const half *__restrict__ B, const int M, const int N, const int K, const size_t pitch)
{
  const int  bx     = blockIdx.x, by = blockIdx.y, tx = threadIdx.x, ty = threadIdx.y;
  const int  warpId = (ty*TC_BLOCK_SIZE_X+tx)/WARP_SIZE, laneId = __fast_mod(tx, WARP_SIZE);
  const int  warpM  = TC_WARP_ROWS*((bx*TC_BLOCK_SIZE_X+tx)/WARP_SIZE);
  const int  warpN  = TC_WARP_COLS*(by*TC_BLOCK_SIZE_Y+ty);
  const int  aRow   = warpM*WMMA_M, bCol = warpN*WMMA_N, cCol = warpN*WMMA_N, cRow = warpM*WMMA_M;
  const int  aSHRow = __fast_mod(warpM, 2*TC_WARP_ROWS)*WMMA_M;
  const int  bSHRow = __fast_mod(warpN, 2*TC_WARP_COLS)*WMMA_N;
  int        announceChannel, blockChannel;

  using namespace nvcuda;
  wmma::fragment<wmma::matrix_a,WMMA_M,WMMA_N,WMMA_K,half,wmma::row_major> a_sh_frag[TC_WARP_ROWS];
  wmma::fragment<wmma::matrix_b,WMMA_M,WMMA_N,WMMA_K,half,wmma::col_major> b_sh_frag[TC_WARP_COLS];
  wmma::fragment<wmma::accumulator,WMMA_M,WMMA_N,WMMA_K,float> acc_sh_frag[TC_BLOCK_WARPS];

  static_assert((nBuff <= 4) && (nBuff >= 0));
  if (threadIdx.z) {
    announceChannel = 0, blockChannel = nBuff;
  } else {
#pragma unroll
    for (int i = 0; i < TC_BLOCK_WARPS; ++i) wmma::fill_fragment(acc_sh_frag[i], 0.0f);
    announceChannel = nBuff, blockChannel = 0;
  }
  /*
    CTA is split into two groups: loaders and consumers. Loaders will continuously feed
    consumers new data. This is done semi-asynchronously by having both groups announce
    to one another when they have finished with a particular batch of data. Loaders need
    not perform these checks for the first nBuff set of batches, as they are writing
    to "fresh" memory locations.
  */
#pragma unroll
  for (int k = 0; k < K; k += WMMA_K) {
    if (threadIdx.z) {
      /* Loader threads */
      const half *aPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(A)+aRow*pitch+laneId*pitch)+k;
      const half *bPtr  = reinterpret_cast<const half*>(reinterpret_cast<const char*>(B)+(size_t)((bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS)))*pitch)+laneId*pitch)+k;
      /*
	For each wmma we need to load in a 64*WMMA_K shm tile = 512 elements. But we have
	only 64 loading threads, so every thread must get 16 coalesced entries. First half
	of warps load A, last half load B, but use vectorized load/stores

	After nBuff loads, loader threads must wait until all consumer threads have used a
	particular batch of shared memory.
      */
      if (k >= nBuff*WMMA_K) __barrier_blocking(blockChannel, TC_BLOCK_BARRIER);
      if (warpId < (TC_BLOCK_WARPS/2)) {
	/* If M is divisible by TC_COMPUTE_SIZE, we can always skip this check */
	if (tcM || (aRow+laneId < M)) {
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(aPtr+i*TC_VEC_LOAD));
	  }
	  if (k != K-WMMA_K) {
	    /* If not at the end of the loop, prefetch the next line */
#pragma unroll
	    for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	      __prefetch_global_l1(aPtr+WMMA_K+i*TC_VEC_LOAD);
	    }
	  }
	} else if (!k) {
	  /* Load zero vector exactly once */
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_A(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	  }
	}
      } else {
	/* Similarly, if N is divisble by WMMA_N this time, we can always skip this check */
	if (tcN || (bCol-(TC_WARP_COLS*WMMA_N*((warpId+1)%TC_WARP_COLS))+laneId < N)) {
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(bPtr+i*TC_VEC_LOAD));
	  }
	  if (k != K-WMMA_K) {
	    /* If not at the end of the loop, prefetch the next line */
#pragma unroll
	    for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	      __prefetch_global_l1(bPtr+WMMA_K+i*TC_VEC_LOAD);
	    }
	  }
	} else if (!k) {
	  /* Also load zero vector exactly once */
#pragma unroll
	  for (int i = 0; i < (WMMA_K/TC_VEC_LOAD); ++i) {
	    *(reinterpret_cast<float4*>(TC_SHMEM_MAT_B(announceChannel)+(tx*(WMMA_K/TC_VEC_LOAD)+i)*TC_VEC_LOAD)) = __ldg(reinterpret_cast<const float4*>(constZeroVector));
	  }
	}
      }
      /*
	Announce to consumer threads that global load is complete, can continue loading
	next chunk immediately (assuming consumers allow it).
      */
      __barrier_nonblocking(announceChannel, TC_BLOCK_BARRIER);
      /*
	Perform predicated modulo within the loops to update which channel the loader
	threads next announce successful loads to.
      */
      announceChannel = __fast_mod(announceChannel+1, nBuff);
      /*
	Only need to update the blocking channel after the first nBuff number of global
	loads, as threads are not overwriting previous loads until then.
      */
      if (k >= nBuff*WMMA_K) blockChannel = nBuff+__fast_mod(blockChannel+1, nBuff);
    } else {
      /*
	Consumer threads

	Consumers must wait until loaders have all produced this batch's
	data. Given the slow (and unfortunately not neatly page-aligned) global memory
	loads, this is the biggest bottleneck.
      */
      __barrier_blocking(blockChannel, TC_BLOCK_BARRIER);
      if (aRow < M && bCol < N) {
#pragma unroll
	for (int i = 0; i < TC_WARP_ROWS; ++i) {
	  /* Load A fragment for every row only once */
	  wmma::load_matrix_sync(a_sh_frag[i], TC_SHMEM_MAT_A(blockChannel)+aSHRow*WMMA_K+i*WMMA_M*WMMA_K, WMMA_K);
#pragma unroll
	  for (int j = 0; j < TC_WARP_COLS; ++j) {
	    /* Loop through column chunks of B, loading only on the first pass */
	    if (!i) wmma::load_matrix_sync(b_sh_frag[j], TC_SHMEM_MAT_B(blockChannel)+bSHRow*WMMA_K+j*WMMA_K*WMMA_N, WMMA_K);
	    wmma::mma_sync(acc_sh_frag[i*TC_WARP_ROWS+j], a_sh_frag[i], b_sh_frag[j], acc_sh_frag[i*TC_WARP_ROWS+j]);
	  }
	}
      }
      /* Consumers announce to producers they are done with this batch */
      __barrier_nonblocking(announceChannel, TC_BLOCK_BARRIER);
      /*
	Here they must update both channels __every__ time. Loader threads will begin
	checking their block channel during the second pass, but this corresponds to the
	consumer threads first pass announcements.
      */
      announceChannel = nBuff+__fast_mod(announceChannel+1, nBuff);
      blockChannel = __fast_mod(blockChannel+1, nBuff);
    }
  }
  /*
    Catch all non-loader threads for final stores. Loader threads take the highway outta
    here.
  */
  if (threadIdx.z) asm volatile ("exit;");
  __barrier_blocking(CTA_CHANNEL, blockDim.x*blockDim.y);
  /*
    Store to shared memory. Note that if K is well-aligned (i.e. K%16 == 0), we could
    simply store directly to global memory here. However, since the shared to global store
    in the main kernel will likely use vectorized stores (something not possible from
    wmma without writing this directly in PTX) this double store is acceptable.
  */
  if (cRow < M && cCol < N && !threadIdx.z) {
#pragma unroll
    for (int i = 0; i < TC_WARP_ROWS; ++i) {
#pragma unroll
      for (int j = 0; j < TC_WARP_COLS; ++j) {
	wmma::store_matrix_sync(TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+i*WMMA_M*TC_COMPUTE_SIZE_X+j*WMMA_K, acc_sh_frag[i*TC_WARP_COLS+j], TC_COMPUTE_SIZE_X, wmma::mem_row_major);
      }
    }
  }
  return;
}

template <bool tcM, bool tcN>
PROJ_HOSTDEVICE __launch_bounds__(TC_BLOCK_SIZE_X*TC_BLOCK_SIZE_Y*TC_BLOCK_SIZE_Z)
void MatMatTransMultBuffered(float *__restrict__ C, const half *__restrict__ A, const half *__restrict__ B, const int M, const int N, const int K, const size_t pitchK, const size_t pitchC)
{
  __shared__ float shmC[TC_COMPUTE_SIZE_X*TC_COMPUTE_SIZE_Y];

  const int   bx    = blockIdx.x, by = blockIdx.y, bz = blockIdx.z;
  const int   tx    = threadIdx.x, ty = threadIdx.y, laneId = __fast_mod(tx, WARP_SIZE);
  const int   warpM = TC_WARP_ROWS*((bx*TC_BLOCK_SIZE_X+tx)/WARP_SIZE);
  const int   warpN = TC_WARP_COLS*(by*TC_BLOCK_SIZE_Y+ty);
  const int   cCol  = warpN*WMMA_N, cRow = warpM*WMMA_M;
  const half  *bPtr = reinterpret_cast<const half*>(reinterpret_cast<const char*>(B)+(size_t)(bz*N*pitchK));

  GEMMOp<tcM,tcN,4>(shmC, A, bPtr, M, N, K, pitchK);
  if (cRow+laneId < M && cCol < N && !threadIdx.z) {
    const int   aSHRow  = __fast_mod(warpM, 2*TC_WARP_ROWS)*WMMA_M;
    const int   bSHRow  = __fast_mod(warpN, 2*TC_WARP_ROWS)*WMMA_N;
    const int   cColRem = min(N-cCol, TC_COMPUTE_SIZE_X/TC_WARP_COLS);
    const float *shcPtr = TC_SHMEM_MAT_C+aSHRow+bSHRow*TC_COMPUTE_SIZE_X+laneId*TC_COMPUTE_SIZE_X;
    float       *cPtr   = reinterpret_cast<float*>(reinterpret_cast<char*>(C)+(size_t)(bz*M*pitchC)+cRow*pitchC+laneId*pitchC)+cCol;

    /*
      Since every warp writes to it's own quadrant of shm, we need not sync the whole
      CTA. However due to voltas independant divergent thread scheduler we also can't
      assume every threads memory transaction has finished, so we must sync here.
    */
    __syncwarp(WARP_MASK);
    /*
      If N is already divisible by WMMA_K then no need to check alignment, everyone
      automatically has an entire line to copy.
    */
    if (tcN) goto max_aligned;
    switch(N%(sizeof(float4)/sizeof(float))) {
      /* 16 byte aligned, use fastest vector instructions */
    case 0:
      /* We are 16 byte aligned, but is there enough to copy? If not, drop to 8 byte */
      if (!(cColRem%(sizeof(float4)/sizeof(float)))) {
      max_aligned:
#pragma unroll
	for (int i = 0; i < cColRem; i += (sizeof(float4)/sizeof(float))) {
	  *(reinterpret_cast<float4*>(cPtr+i)) = *(reinterpret_cast<const float4*>(shcPtr+i));
	}
	break;
      }
      /* 12 byte aligned, use float3. This is mostly untested and may well write OOB */
    case sizeof(float3)/sizeof(float):
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float3)/sizeof(float))) {
	*(reinterpret_cast<float3*>(cPtr+i)) = *(reinterpret_cast<const float3*>(shcPtr+i));
      }
      break;
      /* 8 byte aligned, use float2 vector */
    case sizeof(float2)/sizeof(float):
      /* No need to check here, we check above for odd so if we are here, we are good2go */
#pragma unroll
      for (int i = 0; i < cColRem; i += (sizeof(float2)/sizeof(float))) {
	*(reinterpret_cast<float2*>(cPtr+i)) = *(reinterpret_cast<const float2*>(shcPtr+i));
      }
      break;
      /* Not aligned at all, 4 byte aligned, or have odd number of entries :( */
    default:
#pragma unroll
      for (int i = 0; i < cColRem; ++i) cPtr[i] = shcPtr[i];
      break;
    }
  }
  return;
}

PROJ_HOST_SI size_t roundUp(size_t numToRound, size_t multiple)
{
  ProjFunctionBegin;
  ProjAssertC(multiple && ((multiple&(multiple-1)) == 0));
  ProjFunctionReturn((numToRound+multiple-1)&-multiple);
}

template <int K>
cudaError_t GPUInterface::conv_forward_gpu_TC(float *__restrict__ host_y, const float *__restrict__ host_x, const float *__restrict__ host_k, const int B, const int M, const int C, const int H, const int W)
{
  // Declare relevant device pointers
  const size_t  H_o = H-K+1, W_o = W-K+1, m_size = B*C*K*K*H_o*W_o;
  const size_t  i_size = B*C*H*W, o_size = B*M*H_o*W_o, k_size = M*C*K*K;
  const size_t  minReqMem = m_size*sizeof(half)+(i_size+k_size+o_size)*sizeof(float);
  const size_t  tm = M, tk = C*K*K, tn = H_o*W_o, pitchedK = roundUp(tk, 16)*sizeof(half);
  const int     tmtc = (tm/TC_COMPUTE_SIZE_Y)*TC_COMPUTE_SIZE_Y, tntc = (tn/WMMA_N)*WMMA_N;
  const bool    tcM = tmtc == tm, tcN = tntc == tn;
  const int     kGrid = min(roundUp(tk, WARP_SIZE), (size_t)UNROLL_A_BOUND);
  const int     xGrid = min(roundUp(tk, WARP_SIZE), (size_t)UNROLL_B_BOUND);
  const int     yGrid = min(UNROLL_B_BOUND/xGrid, CTA_CHANNEL);
  const int     xBlocks = (int)ceil((float)tm/(float)(WMMA_M*TC_WARP_ROWS*TC_BLOCK_SIZE_X/WARP_SIZE));
  const int     yBlocks = (int)ceil((float)tn/(float)(WMMA_N*TC_WARP_COLS*TC_BLOCK_SIZE_Y));
  const int     xBlocksX = (int)ceil((float)(C*K*K)/(float)(xGrid));
  const int     xBlocksK = (int)ceil((float)tk/(float)kGrid);
  const dim3    dimGridXStride(xBlocksX, 2*dProp.multiProcessorCount);
  const dim3    dimGridX(xBlocksX, B), dimBlockX(xGrid, yGrid);
  const dim3    dimGridK(xBlocksK, tm), dimBlockK(kGrid);
  const dim3    dimGrid(xBlocks, yBlocks, B), dimBlock(TC_BLOCK_SIZE_X, TC_BLOCK_SIZE_Y, TC_BLOCK_SIZE_Z);
  const	char    buffSize[] = "QUAD\0";

  half         *d_x_half_p, *d_k_half_p;
  float        *d_y_p, *d_x_conv, *d_k_conv;
  size_t       freemem, freemem2, totalmem, pitchC;
  cudaError_t  cerr;

  ProjFunctionBegin;
  /* Preamble */
  cerr = ProjPrintf("%s TENSOR CORE   %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  cerr = ProjPrintf("%s KERNEL START  %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  cerr = ProjPrintf("MAT A: (%d %d) MAT B: (%d %d) x %d MAT C: (%d %d) x %d\n", tm, tk, tk, tn, B, tm, tn, B);CHKERRCE(cerr);
  cerr = ProjPrintf("B %d M %d C %d K %d H %d W %d H_o %d W_o %d\n", B, M, C, K, H, W, H_o, W_o);CHKERRCE(cerr);
  cerr = ProjPrintf("Kernel size %d, const kernel size %d\n", tm*pitchedK/sizeof(half), TC_CONST_A_SIZE);CHKERRCE(cerr);

  /* Sanity checks for various kernel restrictions */
  ProjAssertC(B <= dProp.maxGridSize[2]);
  ProjAssertC(dimBlockK.x*dimBlockK.y*dimBlockK.z <= UNROLL_A_BOUND);
  ProjAssertC(dimBlockX.x*dimBlockX.y*dimBlockX.z <= UNROLL_B_BOUND);

  /* Register memory */
  cerr = GPUInterface::ProjRegisterPointer((void *) host_x, i_size*sizeof(float));CHKERRCE(cerr);
  cerr = GPUInterface::ProjRegisterPointer((void *) host_y, o_size*sizeof(float));CHKERRCE(cerr);
  cerr = GPUInterface::ProjRegisterPointer((void *) host_k, k_size*sizeof(float));CHKERRCE(cerr);

  /* Determine whether we can fit all matrices in memory in one go */
  cerr = cudaMemGetInfo(&freemem, &totalmem);CHKERRCE(cerr);
  cerr = ProjPrintf("%4.2fMB/ %4.2fMB (Free/Total): Minum memory required %4.2fMB\n", freemem/(1024*1024.0), totalmem/(1024*1024.0), minReqMem/(1024*1024.0));CHKERRCE(cerr);
  ProjAssertC(freemem >= minReqMem);

  /* Allocate memory for conversion buffers */
  cerr = cudaMalloc(&d_x_conv, i_size*sizeof(float));CHKERRCE(cerr);
  cerr = cudaMalloc(&d_k_conv, k_size*sizeof(float));CHKERRCE(cerr);
  /* Allocate pitched A memory */
  cerr = cudaMalloc(&d_k_half_p, tm*pitchedK);CHKERRCE(cerr);
  /* Allocate pitched B memory */
  cerr = cudaMalloc(&d_x_half_p, B*tn*pitchedK);CHKERRCE(cerr);
  /* Allocate pitched C memory */
  cerr = cudaMallocPitch(&d_y_p, &pitchC, tn*sizeof(float), B*tm);CHKERRCE(cerr);
  /* Clear pitched input */
  cerr = cudaMemsetAsync(d_k_half_p, 0, tm*pitchedK, stream[0]);CHKERRCE(cerr);
  cerr = cudaMemsetAsync(d_x_half_p, 0, B*tn*pitchedK, stream[1]);CHKERRCE(cerr);
  /* Display memory usage */
  cerr = cudaMemGetInfo(&freemem2, &totalmem);CHKERRCE(cerr);
  cerr = ProjPrintf("%4.2fMB/ %4.2fMB (Free/Total): Actual memory required %4.2fMB\n", freemem2/(1024*1024.0), totalmem/(1024*1024.0), (freemem-freemem2)/(1024*1024.0));CHKERRCE(cerr);

  /* Copy raw data to conversion buffers */
  cerr = cudaMemcpyAsync(d_k_conv, host_k, k_size*sizeof(float), cudaMemcpyHostToDevice, stream[0]);CHKERRCE(cerr);
  cerr = cudaMemcpyAsync(d_x_conv, host_x, i_size*sizeof(float), cudaMemcpyHostToDevice, stream[1]);CHKERRCE(cerr);
  /*
    Because the TA's don't handle async copies on multiple streams. Without explicit sync
    here, OPTime will be measured from the first memcpy to the end of the cudaMemcpy2D
    including CPU overhead...
  */
  cerr = cudaDeviceSynchronize();CHKERRCE(cerr);

  /* Perform conversion for A matrix */
  cerr = ProjPrintf("============ MATRIX A CONVERSION ============\n");CHKERRCE(cerr);
  cerr = ProjPrintf("Launch K conversion: DimGrid (%d,%d,%d) DimBlock (%d,%d,%d)\n", dimGridK.x, dimGridK.y, dimGridK.z, dimBlockK.x, dimBlockK.y, dimBlockK.z);CHKERRCE(cerr);
  cerr = ProjProfileReset();CHKERRCE(cerr);
  cerr = ProjProfileBegin(start, stream[0]);CHKERRCE(cerr);
  setupAMatrix<<<dimGridK, dimBlockK, 0, stream[0]>>>(d_k_half_p, d_k_conv, tk, pitchedK);
  cerr = ProjProfileEnd(start, stop, stream[0]);CHKERRCE(cerr);
  cerr = ProjProfileView();CHKERRCE(cerr);

  /* Perform conversion for B matrix */
#if (1)
  cerr = ProjPrintf("============ MATRIX B CONVERSION ============\n");CHKERRCE(cerr);
  cerr = ProjPrintf("Launch X conversion: DimGrid (%d,%d,%d) DimBlock (%d,%d,%d)\n", dimGridX.x, dimGridX.y, dimGridX.z, dimBlockX.x, dimBlockX.y, dimBlockX.z);CHKERRCE(cerr);
  cerr = ProjPrintf("Shared memory vector version\n");CHKERRCE(cerr);
  cerr = ProjProfileReset();CHKERRCE(cerr);
  for (int i = 0; i < PROJ_PROFILE_REPS; ++i) {
    cerr = ProjProfileBegin(start, stream[1]);CHKERRCE(cerr);
    setupBMatrix<MASK_WIDTH><<<dimGridX, dimBlockX, dimBlockX.x*dimBlockX.y*2*sizeof(half), stream[1]>>>(d_x_half_p, d_x_conv, C, H, W, H_o, W_o, pitchedK, pitchedK/sizeof(half));
    cerr = ProjProfileEnd(start, stop, stream[1]);CHKERRCE(cerr);
  }
  cerr = ProjProfileView();CHKERRCE(cerr);
#else
  cerr = ProjPrintf("Anand Double Buffer version\n");CHKERRCE(cerr);
  cerr = ProjProfileReset();CHKERRCE(cerr);
  for (int i = 0; i < PROJ_PROFILE_REPS; ++i) {
    cerr = ProjProfileBegin(start, stream[1]);CHKERRCE(cerr);
    setupBMatrixDB<MASK_WIDTH><<<dimGridX, dimBlockX, dimBlockX.x*dimBlockX.y*2*sizeof(half), stream[1]>>>(d_x_half_p, d_x_conv, C, H, W, H_o, W_o, pitchedK/sizeof(half));
    cerr = ProjProfileEnd(start, stop, stream[1]);CHKERRCE(cerr);
  }
  cerr = ProjProfileView();CHKERRCE(cerr);
#endif

  cerr = cudaMemcpyToSymbol(constd_a, d_k_half_p, tm*pitchedK, 0, cudaMemcpyDeviceToDevice);CHKERRCE(cerr);

  /* Perform GEMM using tensor cores. Additionally indicate aligned-ness via template args */
  cerr = ProjPrintf("============ TENSOR CORE 64x64 ============\n");CHKERRCE(cerr);
  cerr = ProjPrintf("Launching %s buffered kernel\n", buffSize);CHKERRCE(cerr);
  cerr = ProjPrintf("DimGrid (%d,%d,%d) DimBlock (%d,%d,%d) TCM %s TCN %s\n", dimGrid.x, dimGrid.y, dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z, tcM ? "true" : "false", tcN ? "true" : "false");CHKERRCE(cerr);
  cerr = ProjPrintf("MAT A: (%d %d) pitch %zu (len %d) MAT B: (%d %d) x %d pitch %zu (len %d) MAT C: (%d %d) x %d pitch %zu (len %d)\n", tm, tk, pitchedK, pitchedK/sizeof(half), tk, tn, B, pitchedK, pitchedK/sizeof(half), tm, tn, B, pitchC, pitchC/sizeof(float));CHKERRCE(cerr);
  cerr = ProjProfileReset();CHKERRCE(cerr);
  for (int p = 0; p < PROJ_PROFILE_REPS; ++p) {
    cerr = ProjProfileBegin(start, stream[1]);CHKERRCE(cerr);
    if (tcM) {
      if (tcN) {
	MatMatTransMultBuffered<true,true><<<dimGrid, dimBlock, 0, stream[1]>>>(d_y_p, h_constd_a, d_x_half_p, tm, tn, tk, pitchedK, pitchC);
      }
      MatMatTransMultBuffered<true,false><<<dimGrid, dimBlock, 0, stream[1]>>>(d_y_p, h_constd_a, d_x_half_p, tm, tn, tk, pitchedK, pitchC);
    } else if (tcN) {
      MatMatTransMultBuffered<false,true><<<dimGrid, dimBlock, 0, stream[1]>>>(d_y_p, h_constd_a, d_x_half_p, tm, tn, tk, pitchedK, pitchC);
    } else {
      MatMatTransMultBuffered<false,false><<<dimGrid, dimBlock, 0, stream[1]>>>(d_y_p, h_constd_a, d_x_half_p, tm, tn, tk, pitchedK, pitchC);
    }
    cerr = ProjProfileEnd(start, stop, stream[1]);CHKERRCE(cerr);
  }
  cerr = ProjProfileView();CHKERRCE(cerr);

  /* Retrieve solution and destroy memory. Explicit sync on frees */
  cerr = cudaMemcpy2DAsync(host_y, tn*sizeof(float), d_y_p, pitchC, tn*sizeof(float), B*tm, cudaMemcpyDeviceToHost, stream[1]);CHKERRCE(cerr);
  cerr = cudaFree(d_y_p);CHKERRCE(cerr);
  cerr = cudaFree(d_x_half_p);CHKERRCE(cerr);
  cerr = cudaFree(d_x_conv);CHKERRCE(cerr);
  cerr = cudaFree(d_k_half_p);CHKERRCE(cerr);
  cerr = cudaFree(d_k_conv);CHKERRCE(cerr);
  cerr = ProjPrintf("%s KERNEL END   %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  ProjFunctionReturn(cerr);
}
/* ===================== TENSOR CORE COMPUTE KERNELS ======================= */

/* ===================== SHARED MEMORY COMPUTE KERNELS ===================== */
template <bool isOdd>
PROJ_HOSTDEVICE void arrayConverter(half2 *__restrict__ out, const float2 *__restrict__ in, unsigned int size)
{
#pragma unroll
  for (unsigned int i = blockIdx.x*blockDim.x+threadIdx.x; i < size; i += gridDim.x*blockDim.x) {
    out[i] = __float22half2_rn(in[i]);
  }
  if (isOdd) {
    if (!threadIdx.x && !blockIdx.x) {
      out[size-1] = make_half2(__float2half(in[size-1].x), half(0));
    }
  }
  return;
}

cudaError_t GPUInterface::arrayConvert(half *__restrict__ out, const float *__restrict__ in, unsigned int size, cudaStream_t streamc, int maxBlocks=8)
{
  const dim3  dimGridC(min(max(size/(2*1024), 1), maxBlocks)), dimBlockC(1024);
  const bool  isOdd = size > 2 ? size%2 : size;
  cudaError_t cerr;

  ProjFunctionBegin;
  cerr = ProjPrintf("============ CONVERSION ============\n");CHKERRCE(cerr);
  cerr = ProjPrintf("Launch Conversion: DimGrid (%d,%d,%d) DimBlock (%d,%d,%d), %s\n", dimGridC.x, dimGridC.y, dimGridC.z, dimBlockC.x, dimBlockC.y, dimBlockC.z, isOdd ? "Odd" : "Even");CHKERRCE(cerr);
  cerr = ProjProfileReset();CHKERRCE(cerr);
  for (int p = 0; p < PROJ_PROFILE_REPS; ++p) {
    cerr = ProjProfileBegin(start, streamc);CHKERRCE(cerr);
    if (isOdd) {
      arrayConverter<true><<<dimGridC,dimBlockC,0,streamc>>>((half2*)out,(const float2*)in,size/2);
    } else {
      arrayConverter<false><<<dimGridC,dimBlockC,0,streamc>>>((half2*)out,(const float2*)in,size/2);
    }
    cerr = ProjProfileEnd(start, stop, streamc);CHKERRCE(cerr);
  }
  cerr = ProjProfileView();CHKERRCE(cerr);
  ProjFunctionReturn(cudaSuccess);
}

#if defined(BLOCK_RESHUFFLE)
template <int K, typename T>
PROJ_HOSTDEVICE __launch_bounds__(BLOCK_SIZE_X*BLOCK_SIZE_Y*BLOCK_SIZE_Z)
void conv_forward_kernel_const(float *__restrict__ y, const T *__restrict__  x, const int B, const int M, const int C, const int H, const int W, const int H_out, const int W_out)
{
  /* Include the aprons, note that dimBlock = {BLOCK_SIZE x BLOCK_SIZE} = {32 x 32} */
  __shared__ half tile[BLOCK_SIZE_Z*(BLOCK_SIZE_X+MASK_WIDTH-1)*(BLOCK_SIZE_Y+MASK_WIDTH-1)];

  const int tx = threadIdx.x, ty = threadIdx.y, bx = blockIdx.x, by = blockIdx.y, b = blockIdx.z;
  const int xo = BLOCK_SIZE_X*bx+tx, yo = BLOCK_SIZE_Y*by+ty;

#pragma unroll
  for (int m = 0; m < M; ++m) {
    half result = half(0);
#pragma unroll
    for (int c = 0; c < C; ++c) {
      __syncthreads();
      /* Load */
#pragma unroll
      for (int i = 0; i < 2; ++i) {
	const int xi = xo+i*BLOCK_SIZE_X, txid = tx+i*BLOCK_SIZE_X;
	if ((xi < W) && (txid < BLOCK_SIZE_X+K-1)) {
#pragma unroll
	  for (int j = 0; j < 2; ++j) {
	    const int yi = yo+j*BLOCK_SIZE_Y, tyid = ty+j*BLOCK_SIZE_Y;
	    if ((tyid < BLOCK_SIZE_Y+K-1) && (yi < H)) T2D(tyid,txid) = GX4D(b,c,yi,xi);
	  }
	}
      }
      __syncthreads();
      /* Compute */
      if (yo < H_out && xo < W_out) {
#pragma unroll
	for (int yk = 0; yk < K; ++yk) {
#pragma unroll
	  for (int xk = 0; xk < K; ++xk) {
	    result = __hfma(T2D(ty+yk,tx+xk),CK4DH(m,c,yk,xk),result);
	  }
	}
      }
    }
    /* Yeet */
    if (yo < H_out && xo < W_out) Y4D(b,m,yo,xo) = __half2float(result);
  }
  return;
}
#else
template <int K, typename T>
PROJ_HOSTDEVICE __launch_bounds__(BLOCK_SIZE_X*BLOCK_SIZE_Y*BLOCK_SIZE_Z)
void conv_forward_kernel_const(T *__restrict__ y, const T *__restrict__  x, const int B, const int M, const int C, const int H, const int W, const int H_out, const int W_out)
{
  /* Include the aprons, note that dimBlock = {BLOCK_SIZE x BLOCK_SIZE} = {32 x 32} */
  __shared__ T tile[BLOCK_SIZE_Z*(BLOCK_SIZE_X+MASK_WIDTH-1)*(BLOCK_SIZE_Y+MASK_WIDTH-1)];

  const int tx = threadIdx.x, ty = threadIdx.y, bx = blockIdx.x, by = blockIdx.y, bz = blockIdx.z;
  const int xo = BLOCK_SIZE_X*bx+tx, yo = BLOCK_SIZE_Y*by+ty;

  /* Load */
#pragma unroll
  for (int m = 0; m < M; ++m) {
    T result = T(0);
#pragma unroll
    for (int c = 0; c < C; ++c) {
      __syncthreads();
#pragma unroll
      for (int i = 0; i < 2; ++i) {
#pragma unroll
	for (int j = 0; j < 2; ++j) {
	  const int xi = xo+i*BLOCK_SIZE_X, yi = yo+j*BLOCK_SIZE_Y;
	  const int txid = tx+i*BLOCK_SIZE_X, tyid = ty+j*BLOCK_SIZE_Y;
	  if ((txid < BLOCK_SIZE_X+K-1) && (tyid < BLOCK_SIZE_Y+K-1)) {
	    if (xi < W && yi < H) T2D(tyid,txid) = GX4D(bz,c,yi,xi);
	  }
	}
      }
      __syncthreads();
      if (yo < H_out && xo < W_out) {
#pragma unroll
	for (int yk = 0; yk < K; ++yk) {
#pragma unroll
	  for (int xk = 0; xk < K; ++xk) {
	    result = __fmaf_rn(T2D(ty+yk,tx+xk),CK4D(m,c,yk,xk),result);
	  }
	}
      }
    }
    if (yo < H_out && xo < W_out) Y4D(bz,m,yo,xo) = result;
  }
  return;
}
#endif

template <int K>
cudaError_t GPUInterface::conv_forward_gpu_SH(float *__restrict__ host_y, const float *__restrict__ host_x, const float *__restrict__ host_k, const int B, const int M, const int C, const int H, const int W)
{
  // Declare relevant device pointers
  const int    H_out  = H-K+1, W_out = W-K+1;
  const int    o_size = B*M*H_out*W_out, i_size = B*C*H*W, k_size = M*C*K*K;
  const int    xBlocks = (int)ceil((float)W_out/(float)BLOCK_SIZE_X);
  const int    yBlocks = (int)ceil((float)H_out/(float)BLOCK_SIZE_Y);
  const dim3   dimGrid(xBlocks, yBlocks, B), dimBlock(BLOCK_SIZE_X, BLOCK_SIZE_Y, BLOCK_SIZE_Z);
  float        *d_y, *d_x;
  half         *d_k_h, *d_x_h;
  cudaError_t  cerr;

  ProjFunctionBegin;
  /* Preamble */
  ProjAssertC(B <= dProp.maxGridSize[2]);
  cerr = ProjPrintf("%s SHARED MEMORY %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  cerr = ProjPrintf("%s KERNEL START  %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  cerr = ProjPrintf("DimGrid (%d,%d,%d) DimBlock (%d,%d,%d)\n", dimGrid.x, dimGrid.y, dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);CHKERRCE(cerr);
  cerr = ProjPrintf("B %d M %d C %d H %d W %d K %d\n", B, M, C, H, W, K);CHKERRCE(cerr);
  cerr = ProjPrintf("Kernel size %d, const kernel size %d\n", k_size, KERNEL_SIZE);CHKERRCE(cerr);

  /* Allocate memory */
  ProjAssertC(K < dProp.warpSize);
  cerr = cudaMalloc(&d_x, i_size*sizeof(float));CHKERRCE(cerr);
  cerr = cudaMalloc(&d_y, o_size*sizeof(float));CHKERRCE(cerr);
  cerr = cudaMalloc(&d_k_h, k_size*sizeof(half));CHKERRCE(cerr);
  cerr = cudaMalloc(&d_x_h, i_size*sizeof(half));CHKERRCE(cerr);

  /* Register memory */
  cerr = GPUInterface::ProjRegisterPointer((void *) host_x, i_size*sizeof(float));CHKERRCE(cerr);
  cerr = GPUInterface::ProjRegisterPointer((void *) host_y, o_size*sizeof(float));CHKERRCE(cerr);
  cerr = GPUInterface::ProjRegisterPointer((void *) host_k, k_size*sizeof(float));CHKERRCE(cerr);

  /* Copy to device */
  cerr = cudaMemcpyAsync(d_x, host_x, i_size*sizeof(float), cudaMemcpyHostToDevice, stream[0]);CHKERRCE(cerr);

  /* *slaps roof of constant memory* this address can fit so many kernels in it */
  cerr = cudaMemcpyToSymbolAsync(constd_k, host_k, k_size*sizeof(float), 0, cudaMemcpyHostToDevice, stream[0]);CHKERRCE(cerr);
#if (0)
  float mintime[3] = {123456.0f, 123456.0f, 123456.0f};
  int   ilo[3]     = {-1, -1, -1};
  for (int i = 2; i < 1000; i += 2) {
    float t = 123456.0f;

    cerr = arrayConvert<false>(d_x_h, d_x, i_size, stream[0], i);CHKERRCE(cerr);
    cerr = ProjProfileGetInfo(NULL, &t, NULL, NULL);CHKERRCE(cerr);
    for (int j = 0; j < 3; ++j) {
      if ((t/(float)PROJ_PROFILE_REPS) < mintime[j]) {
	for (int k = 2; k > j; --k) {
	  mintime[k] = mintime[k-1];
	  ilo[k] = ilo[k-1];
	}
	mintime[j] = t;
	ilo[j] = i;
	break;
      }
    }
    cerr = cudaDeviceSynchronize();CHKERRCE(cerr);
  }
  cerr = ProjPrintf("Fastest iteration %d: %f\n", ilo[0], mintime[0]);CHKERRCE(cerr);
  cerr = ProjPrintf("Second Fastest iteration %d: %f\n", ilo[1], mintime[1]);CHKERRCE(cerr);
  cerr = ProjPrintf("Third Fastest iteration %d: %f\n", ilo[2], mintime[2]);CHKERRCE(cerr);
#else
  cerr = arrayConvert(d_x_h, d_x, i_size, stream[0], B);CHKERRCE(cerr);
#endif
  cerr = arrayConvert(d_k_h, h_constd_k, k_size, stream[0], 8);CHKERRCE(cerr);

  cerr = cudaMemcpyToSymbolAsync(constd_k_h, d_k_h, k_size*sizeof(half), 0, cudaMemcpyDeviceToDevice, stream[0]);CHKERRCE(cerr);

  /* Compute kernel */
  cerr = ProjProfileReset();CHKERRCE(cerr);
  for (int p = 0; p < PROJ_PROFILE_REPS; ++p) {
    cerr = ProjProfileBegin(start, stream[0]);CHKERRCE(cerr);
    conv_forward_kernel_const<K><<<dimGrid,dimBlock,0,stream[0]>>>(d_y,d_x_h,B,M,C,H,W,H_out,W_out);
    cerr = ProjProfileEnd(start, stop, stream[0]);CHKERRCE(cerr);
  }
  cerr = ProjProfileView();CHKERRCE(cerr);

  /* Return and cleanup, explicit sync on frees */
  cerr = cudaMemcpyAsync(host_y, d_y, o_size*sizeof(float), cudaMemcpyDeviceToHost, stream[0]);CHKERRCE(cerr);
  cerr = cudaFree(d_y);CHKERRCE(cerr);
  cerr = cudaFree(d_x);CHKERRCE(cerr);
  cerr = cudaFree(d_k_h);CHKERRCE(cerr);
  cerr = cudaFree(d_x_h);CHKERRCE(cerr);
  cerr = ProjPrintf("%s KERNEL END   %s\n", print_bar__, print_bar__);CHKERRCE(cerr);
  ProjFunctionReturn(cudaSuccess);
}
/* ===================== SHARED MEMORY COMPUTE KERNELS ===================== */
void GPUInterface::conv_forward_gpu(float *host_y, const float *host_x, const float *host_k, const int B, const int M, const int C, const int H, const int W, const int K)
{
  cudaError_t cerr;

  ProjFunctionBegin;
  ProjAssert(MASK_WIDTH == K);
#if defined(PROJ_USE_DEBUG)
  cerr = conv_forward_gpu_TC<MASK_WIDTH>(host_y,host_x,host_k,B,M,C,H,W);CHKERRC(cerr);
  //cerr = conv_forward_gpu_SH<MASK_WIDTH>(host_y,host_x,host_k,B,M,C,H,W);CHKERRC(cerr);
#else
  if ((H <= 2*WARP_SIZE) && (W <= 2*WARP_SIZE)) {
    cerr = conv_forward_gpu_TC<MASK_WIDTH>(host_y,host_x,host_k,B,M,C,H,W);CHKERRC(cerr);
  } else {
    cerr = conv_forward_gpu_SH<MASK_WIDTH>(host_y,host_x,host_k,B,M,C,H,W);CHKERRC(cerr);
  }
#endif
  ProjFunctionReturnVoid;
}

void GPUInterface::get_device_properties()
{
  int         deviceCount;
  cudaError_t cerr;

  ProjFunctionBegin;
  cerr = cudaGetDeviceCount(&deviceCount);CHKERRC(cerr);
  for(int dev = 0; dev < deviceCount; ++dev) {
    cudaDeviceProp deviceProp;

    cerr = cudaGetDeviceProperties(&deviceProp, dev);CHKERRC(cerr);
    cerr = cudaGetDeviceProperties(&dProp, dev);CHKERRC(cerr);
    if (ProjDevicePropPrinted) continue;
#if defined(PROJ_USE_DEBUG)
    std::cout<<"Device "<<dev<<" name: "<<deviceProp.name<<std::endl;
    std::cout<<"Computational capabilities: "<<deviceProp.major<<"."<<deviceProp.minor<<std::endl;
    std::cout<<"Max Global memory size: "<<deviceProp.totalGlobalMem<<std::endl;
    std::cout<<"Max Constant memory size: "<<deviceProp.totalConstMem<<std::endl;
    std::cout<<"Max Shared memory size per block: "<<deviceProp.sharedMemPerBlock<<std::endl;
    std::cout<<"Max threads per block: "<<deviceProp.maxThreadsPerBlock<<std::endl;
    std::cout<<"Max block dimensions: "<<deviceProp.maxThreadsDim[0]<<" x, "<<deviceProp.maxThreadsDim[1]<<" y, "<<deviceProp.maxThreadsDim[2]<<" z"<<std::endl;
    std::cout<<"Max grid dimensions: "<<deviceProp.maxGridSize[0]<<" x, "<<deviceProp.maxGridSize[1]<<" y, "<<deviceProp.maxGridSize[2]<<" z"<<std::endl;
    std::cout<<"Warp Size: "<<deviceProp.warpSize<<std::endl;
    std::cout<<"Supports unified addressing: "<<deviceProp.unifiedAddressing<<std::endl;
    std::cout<<"Number of asynchronous copy engines: "<<deviceProp.asyncEngineCount<<std::endl;
    std::cout<<"SM Count: "<<deviceProp.multiProcessorCount<<std::endl;
#endif
  }
  ProjAssertV(dProp.warpSize == WARP_SIZE);
  ProjDevicePropPrinted = true;
  ProjFunctionReturnVoid;
}

GPUInterface::GPUInterface(void)
{
  cudaError_t cerr;

  /* Init stack first */
  cerr = ProjStackInitialize();CHKERRC(cerr);
  ProjFunctionBegin;
  cerr = cudaDeviceSynchronize();CHKERRC(cerr);
  for (int i = 0; i < PROJ_MAX_STREAMS; ++i) {
    cerr = cudaStreamCreate(&stream[i]);CHKERRC(cerr);
    cerr = ProjPrintf("Created cuda stream %d\n", i);CHKERRC(cerr);
  }
  for (int i = 0; i < PROJ_MAX_REG_PTRS; ++i) regptrs[i] = NULL;
  cerr = cudaEventCreateWithFlags(&copyEvent, cudaEventDisableTiming);CHKERRC(cerr);
  cerr = cudaEventCreateWithFlags(&computeEvent, cudaEventDisableTiming);CHKERRC(cerr);
  nreg = -1;
  GPUInterface::get_device_properties();
  if (!zeroVecInit) {
    half loczero[TC_VEC_LOAD] = {};

    cerr = cudaMemcpyToSymbol(constZeroVector, loczero, TC_VEC_LOAD*sizeof(half), 0, cudaMemcpyHostToDevice);CHKERRC(cerr);
    zeroVecInit = true;
  }
  cerr = ProjProfileInitialize(&start, &stop);CHKERRC(cerr);
  cerr = ProjPrintf("sizeof(half) %zu sizeof(float) %zu, sizeof(double) %zu sizeof(int) %zu\n", sizeof(half), sizeof(float), sizeof(double), sizeof(int));CHKERRC(cerr);
  cerr = cudaGetSymbolAddress((void **) &h_constd_k, constd_k);CHKERRC(cerr);
  cerr = cudaGetSymbolAddress((void **) &h_constd_k_h, constd_k_h);CHKERRC(cerr);
  cerr = cudaGetSymbolAddress((void **) &h_constd_a, constd_a);CHKERRC(cerr);
  ProjFunctionReturnVoid;
}

GPUInterface::~GPUInterface(void)
{
  cudaError_t cerr;

  ProjFunctionBegin;
  cerr = cudaDeviceSynchronize();CHKERRC(cerr);
  for (int i = 0; i < PROJ_MAX_STREAMS; ++i) {
    cerr = cudaStreamDestroy(stream[i]);CHKERRC(cerr);
    cerr = ProjPrintf("Destroyed cuda stream %d\n", i);CHKERRC(cerr);
  }
  cerr = cudaEventDestroy(copyEvent);CHKERRC(cerr);
  cerr = cudaEventDestroy(computeEvent);CHKERRC(cerr);
  for (; nreg >= 0; --nreg) {
    cerr = GPUInterface::ProjUnregisterPointer(regptrs[nreg]);CHKERRC(cerr);
  }
  cerr = ProjProfileFinalize(start, stop);CHKERRC(cerr);
  cerr = ProjStackFinalize();CHKERRC(cerr);
  return;
}
