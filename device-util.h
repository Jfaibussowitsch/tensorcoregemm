#ifndef DEVICE_UTIL_H
#define DEVICE_UTIL_H

#include "gpu-new-forward.h"

template <typename Ti, typename Tm>
PROJ_DEVICE_SI constexpr int __fast_mod(const Ti in, const Tm mod)
{
  return in >= mod ? in%mod : in;
}

PROJ_DEVICE_SI void __barrier_nonblocking(const int id, const int nt)
{
  asm volatile ("barrier.arrive %0, %1;" :: "r"(id), "r"(nt) : "memory");
}

PROJ_DEVICE_SI void __barrier_blocking(const int id, const int nt)
{
  asm volatile ("barrier.sync %0, %1;" :: "r"(id) , "r"(nt): "memory");
}

PROJ_DEVICE_SI void __prefetch_global_l1(const void* const ptr)
{
  asm ("prefetch.global.L1 [%0];" :: "l"(ptr));
}

PROJ_DEVICE_SI void __prefetch_global_uniform(const void* const ptr)
{
  asm("prefetchu.L1 [%0];" :: "l"(ptr));
}

PROJ_DEVICE_SI void __prefetch_global_l2(const void* const ptr)
{
  asm("prefetch.global.L2 [%0];" :: "l"(ptr));
}

PROJ_DEVICE_SI float dot(float3 a, float3 b)
{
  return a.x*b.x+a.y*b.y+a.z*b.z;
}

PROJ_DEVICE_SI float dot(float4 a, float4 b)
{
  return a.x*b.x+a.y*b.y+a.z*b.z+a.w*b.w;
}
#endif
